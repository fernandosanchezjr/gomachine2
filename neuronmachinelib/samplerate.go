package neuronmachinelib

import (
	"fmt"
	"github.com/unixpickle/wav"
	"math"
	"time"
)

type SampleRate struct {
	SamplePos
	*Frequency
	SamplesPerWavelength SamplePos
}

func NewSampleRate(rate int, f *Frequency) *SampleRate {
	sr := &SampleRate{SamplePos: NewSamplePos(rate), Frequency: f}
	sr.SamplesPerWavelength = sr.Frequency.SamplesPerWavelength(sr.SamplePos)
	return sr
}

func (sr SampleRate) String() string {
	return fmt.Sprintf("%s samples, %s Hz center, %s samples per wavelength",
		sr.SamplePos, sr.Frequency, sr.SamplesPerWavelength)
}

func (sr SampleRate) AnglePercent(o SamplePos) float64 {
	return sr.SamplesPerWavelength.AnglePercent(o)
}

func (sr SampleRate) AngleRadians(o SamplePos) float64 {
	return sr.SamplesPerWavelength.AngleRadians(o)
}

func (sr SampleRate) CentralNeuron() *NeuronId {
	center := NewSamplePosFromFloat(sr.SamplesPerWavelength.Float() / 2.0)
	return NewNeuronId(center, NewRadius(center.Float()), NewRadius(sr.SamplesPerWavelength.Float()*0.05))
}

func (sr SampleRate) GenerateSineSamples(d time.Duration) []wav.Sample {
	totalSamples := sr.SampleRateXTime(time.Duration(d))
	samples := make([]wav.Sample, totalSamples.Int())
	i := NewSamplePos(0)
	for ; i.LessThan(totalSamples); i.Value++ {
		samples[i.Int()] = wav.Sample(math.Sin(sr.AngleRadians(i)))
	}
	return samples
}

func (sr SampleRate) GenerateSine(d time.Duration) wav.Sound {
	samples := sr.GenerateSineSamples(time.Duration(d))
	sound := wav.NewPCM8Sound(1, sr.SamplePos.Int())
	sound.SetSamples(samples)
	return sound
}
