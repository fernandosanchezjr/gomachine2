package neuronmachinelib

import (
	"encoding/gob"
	"github.com/fernandosanchezjr/anko/vm"
	"github.com/unixpickle/wav"
	"reflect"
)

func Imports(env *vm.Env) *vm.Env {
	m := env.NewPackage("neuronmachine")

	var f Frequency
	var sp SamplePos
	var sr SampleRate
	var r Radius
	var n NeuronId
	var wn WaveformNeuron
	var sw SampleWindow
	var ws wav.Sample
	var e Ear

	gob.Register(&f)
	gob.Register(&sp)
	gob.Register(&sr)
	gob.Register(&r)
	gob.Register(&n)
	gob.Register(&wn)
	gob.Register(&sw)
	gob.Register(&ws)
	gob.Register(&e)

	m.DefineType("Frequency", reflect.TypeOf(f))
	m.DefineType("SamplePos", reflect.TypeOf(sp))
	m.DefineType("SampleRate", reflect.TypeOf(sr))
	m.DefineType("Radius", reflect.TypeOf(r))
	m.DefineType("NeuronId", reflect.TypeOf(n))
	m.DefineType("WaveformNeuron", reflect.TypeOf(wn))
	m.DefineType("SampleWindow", reflect.TypeOf(sw))
	m.DefineType("Sample", reflect.TypeOf(ws))
	m.DefineType("Ear", reflect.TypeOf(e))

	m.Define("NewFrequency", NewFrequency)
	m.Define("NewRadius", NewRadius)
	m.Define("NewSamplePos", NewSamplePos)
	m.Define("NewSamplePosFromFloat", NewSamplePosFromFloat)
	m.Define("NewSampleRate", NewSampleRate)
	m.Define("NewWaveformNeuron", NewWaveformNeuron)
	m.Define("NewSampleWindow", NewSampleWindow)
	m.Define("NewEar", NewEar)

	return m
}
