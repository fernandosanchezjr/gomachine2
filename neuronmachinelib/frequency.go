package neuronmachinelib

import (
	"fmt"
	"github.com/unixpickle/wav"
	"gomachine"
	"math"
	"time"
)

type Frequency struct {
	Value float64
}

func NewFrequency(value float64) *Frequency {
	return &Frequency{Value: value}
}

func (f Frequency) Int() int {
	return int(math.Floor(f.Value))
}

func (f Frequency) Float() float64 {
	return f.Value
}

func (f Frequency) SamplesPerWavelength(s SamplePos) SamplePos {
	return NewSamplePosFromFloat(s.Float() / f.Float())
}

func (f Frequency) Equals(o Frequency) bool {
	return f.Float() == o.Float()
}

func (f Frequency) String() string {
	return fmt.Sprint(f.Float())
}

func (f Frequency) SetupSensingTree(bin gomachine.Bin) {
	sr := NewSampleRate(11025, &f)
	bin.Put("sample_rate", sr)
	fmt.Println("Setting up sensing tree for", sr, "at", bin.Did())
	neuronId := sr.CentralNeuron()
	centerNeuron := bin.AddChild(neuronId)
	neuronId.SetupNeuron(centerNeuron)
	bin.Persist()
}

func (f Frequency) Train(bin gomachine.Bin) {
	rawSr, _ := bin.Get("sample_rate")
	sr := rawSr.(*SampleRate)
	sound := sr.GenerateSine(1 * time.Second)
	sw := NewSampleWindow(sound, sr.SamplesPerWavelength.Int())
	children := bin.Children()
	for id, child := range children {
		id.(*NeuronId).Train(child, sw.Clone())
	}
}

func (f Frequency) Evaluate(bin gomachine.Bin, samples []wav.Sample) []wav.Sample {
	rawSr, _ := bin.Get("sample_rate")
	sr := rawSr.(*SampleRate)
	neuronId := sr.CentralNeuron()
	centerNeuron := bin.AddChild(neuronId)
	sw := NewSampleWindowFromSamples(samples, sr.SamplesPerWavelength.Int())
	returnSamples := make([]wav.Sample, len(samples))
	returnPos := 0
	for {
		returnPos = sw.Start
		if s, ok := sw.NextWindowOffset(10); !ok {
			break
		} else {
			returnSamples[returnPos] = wav.Sample(neuronId.Evaluate(centerNeuron, s))
		}
	}
	return returnSamples
}
