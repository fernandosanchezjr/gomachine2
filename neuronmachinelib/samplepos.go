package neuronmachinelib

import (
	"fmt"
	"math"
	"time"
)

type SamplePos struct {
	Value int
}

func NewSamplePos(value int) SamplePos {
	return SamplePos{Value: value}
}

func NewSamplePosFromFloat(value float64) SamplePos {
	return SamplePos{Value: int(math.Floor(value))}
}

func (s SamplePos) Int() int {
	return s.Value
}

func (s SamplePos) Float() float64 {
	return float64(s.Value)
}

func (s SamplePos) SampleRateXTime(t time.Duration) SamplePos {
	return NewSamplePosFromFloat(t.Seconds() * s.Float())
}

func (s SamplePos) LessThan(o SamplePos) bool {
	return s.Int() < o.Int()
}

func (s SamplePos) GreaterThan(o SamplePos) bool {
	return s.Int() > o.Int()
}

func (s SamplePos) Equals(o SamplePos) bool {
	return s.Int() == o.Int()
}

func (s SamplePos) String() string {
	return fmt.Sprint(s.Int())
}

func (s SamplePos) AnglePercent(o SamplePos) float64 {
	return math.Mod(o.Float(), s.Float()) / s.Float()
}

func (s SamplePos) AngleRadians(o SamplePos) float64 {
	return s.AnglePercent(o) * (math.Pi * 2)
}

func (s SamplePos) Instant(o SamplePos) float64 {
	return o.Float() / s.Float()
}
