package neuronmachinelib

import (
	"fmt"
	"gomachine/ioutil"
	"testing"
	"time"
)

func TestSineWave(t *testing.T) {
	sine1 := NewTrigWaveform(60, 8000, time.Duration(time.Second*1))
	//sine2 := NewTrigWaveform(80, 8000, time.Duration(time.Second * 1))

	wn1 := NewWaveformNeuron(2)
	wn1.TrainFromWaveform(sine1, 0, 0.25)
	wn2 := NewWaveformNeuron(2)
	wn2.TrainFromWaveform(sine1, 0.25, 0.5)
	wn3 := NewWaveformNeuron(2)
	wn3.TrainFromWaveform(sine1, 0.5, 0.75)
	wn4 := NewWaveformNeuron(2)
	wn4.TrainFromWaveform(sine1, 0.75, 1.0)

	sine1.Write(ioutil.CreateFile("sin1.wav"))
	//sine2.Write(ioutil.CreateFile("sin2.wav"))

	wn1.ProcessWaveform(sine1).Write(ioutil.CreateFile("process1-1.wav"))
	wn2.ProcessWaveform(sine1).Write(ioutil.CreateFile("process1-2.wav"))
	wn3.ProcessWaveform(sine1).Write(ioutil.CreateFile("process1-3.wav"))
	wn4.ProcessWaveform(sine1).Write(ioutil.CreateFile("process1-4.wav"))

	//wn1.ProcessWaveform(sine2).Write(ioutil.CreateFile("process2-1.wav"))
	//wn2.ProcessWaveform(sine2).Write(ioutil.CreateFile("process2-2.wav"))
	//wn3.ProcessWaveform(sine2).Write(ioutil.CreateFile("process2-3.wav"))
	//wn4.ProcessWaveform(sine2).Write(ioutil.CreateFile("process2-4.wav"))
}

func TestBasicFunctions(t *testing.T) {
	sr := NewSampleRate(8000, NewFrequency(60.0))
	fmt.Println("Sample rate:", sr)
	sound := sr.GenerateSine(1 * time.Second)
	sound.Write(ioutil.CreateFile("TestBasicFunctions-1.wav"))
	neuronId := sr.CentralNeuron()
	fmt.Println("Starting from", neuronId)
	for {
		left, right, split := neuronId.Split()
		if !split {
			break
		} else {
			fmt.Println("Subdivided to", left, "and", right)
			neuronId = left
		}
	}
}

func TestSampleWindow(t *testing.T) {
	sr := NewSampleRate(8000, NewFrequency(60.0))
	sound := sr.GenerateSine(10 * time.Second)
	sw := NewSampleWindow(sound, sr.SamplesPerWavelength.Int())
	fmt.Println(sw)
	for {
		_, ok := sw.NextWindow()
		if !ok {
			break
		}
	}
}
