package neuronmachinelib

import (
	"fmt"
	"github.com/unixpickle/wav"
	"gomachine"
)

type Ear struct {
	Name string
}

func NewEar(name string) *Ear {
	return &Ear{Name: name}
}

func (ear Ear) SetupFrequency(bin gomachine.Bin, frequency float64) {
	freq := NewFrequency(frequency)
	freqBin := bin.AddChild(freq)
	freq.SetupSensingTree(freqBin)
	bin.Persist()
}

func (ear Ear) Train(bin gomachine.Bin) {
	fmt.Println("Training ear", ear)
	children := bin.Children()
	for id, child := range children {
		id.(*Frequency).Train(child)
	}
}

func (ear Ear) Evaluate(bin gomachine.Bin, sound wav.Sound) map[float64][]wav.Sample {
	children := bin.Children()
	fmt.Println("Evaluating at ear", ear)
	results := make(map[float64][]wav.Sample)
	for id, child := range children {
		freq := id.(*Frequency)
		results[freq.Float()] = freq.Evaluate(child, sound.Samples())
	}
	return results
}
