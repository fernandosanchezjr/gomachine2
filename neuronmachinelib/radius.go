package neuronmachinelib

import "math"

type Radius struct {
	Value float64
}

func NewRadius(value float64) Radius {
	return Radius{Value: value}
}

func (p Radius) Plus(center float64) float64 {
	return center + p.Value
}

func (p Radius) Minus(center float64) float64 {
	return center - p.Value
}

func (p Radius) Float() float64 {
	return p.Value
}

func (p Radius) Int() int {
	return int(math.Floor(float64(p.Value)))
}

func (p Radius) Halve() Radius {
	return NewRadius(p.Float() / 2.0)
}

func (s Radius) LessThan(o Radius) bool {
	return s.Float() < o.Float()
}

func (s Radius) GreaterThan(o Radius) bool {
	return s.Float() > o.Float()
}

func (s Radius) Equals(o Radius) bool {
	return s.Float() == o.Float()
}
