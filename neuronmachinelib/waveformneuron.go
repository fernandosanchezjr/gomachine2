package neuronmachinelib

import (
	"errors"
	"github.com/goml/gobrain"
	"math"
)

type WaveformNeuron struct {
	TestIterations int
	LRate          float64
	MFactor        float64
	Brain          *gobrain.FeedForward
	TrainingData   [][][]float64
	Inputs         int
}

func NewWaveformNeuron(inputs int) *WaveformNeuron {
	ret := &WaveformNeuron{TestIterations: 100000, LRate: 0.6, MFactor: 0.4, Brain: &gobrain.FeedForward{},
		Inputs: inputs}
	ret.ClearTrainingData()
	ret.Brain.Init(inputs, inputs*3, 1)
	return ret
}

func (wn *WaveformNeuron) AddTrainingEntry(result float64, inputs ...float64) {
	if len(inputs) != wn.Inputs {
		panic(errors.New("Invalid input count for update"))
	}
	wn.TrainingData = append(wn.TrainingData, [][]float64{inputs, []float64{result}})
}

func (wn *WaveformNeuron) Train(iterations int, lRate float64, mFactor float64, debug bool) []float64 {
	return wn.Brain.Train(wn.TrainingData, iterations, lRate, mFactor, debug)
}

func (wn *WaveformNeuron) ClearTrainingData() {
	wn.TrainingData = make([][][]float64, 0)
}

func (wn *WaveformNeuron) TrainFromWaveform(waveform *TrigWaveform, acceptStart float64, acceptEnd float64) {
	for i := 0; i < waveform.SampleLen(); i++ {
		fi := waveform.Angle(i)
		val := math.Abs(waveform.GetSample(i))
		if acceptStart <= fi && fi < acceptEnd {
			wn.AddTrainingEntry(1.0, fi, val)
		} else {
			wn.AddTrainingEntry(0.0, fi, val)
		}
	}
	wn.PerformTraining()
}

func (wn *WaveformNeuron) PerformTraining() {
	if len(wn.TrainingData) > 0 {
		wn.Train(wn.TestIterations, wn.LRate, wn.MFactor, true)
		wn.Brain.Test(wn.TrainingData)
		wn.ClearTrainingData()
	}
}

func (wn *WaveformNeuron) Update(inputs ...float64) float64 {
	if len(inputs) != wn.Inputs {
		panic(errors.New("Invalid input count for update"))
	}
	result := wn.Brain.Update(inputs)
	return result[0]
}

func (wn WaveformNeuron) ProcessWaveform(waveform *TrigWaveform) *TrigWaveform {
	ret := waveform.Clone()
	for i := 0; i < waveform.SampleLen(); i++ {
		fi := waveform.Angle(i)
		val := math.Abs(waveform.GetSample(i))
		result := wn.Update(fi, val)
		ret.SetSample(i, result)
	}
	return ret
}
