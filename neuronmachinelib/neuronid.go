package neuronmachinelib

import (
	"fmt"
	"github.com/unixpickle/wav"
	"gomachine"
	"math"
)

type NeuronId struct {
	Center    SamplePos
	Radius    Radius
	MinRadius Radius
}

func NewNeuronId(center SamplePos, radius Radius, minRadius Radius) *NeuronId {
	return &NeuronId{Center: center, Radius: radius, MinRadius: minRadius}
}

func (n NeuronId) Split() (*NeuronId, *NeuronId, bool) {
	newRadius := n.Radius.Halve()
	if newRadius.GreaterThan(n.MinRadius) {
		left := NewNeuronId(NewSamplePosFromFloat(newRadius.Minus(n.Center.Float())),
			newRadius, n.MinRadius)
		right := NewNeuronId(NewSamplePosFromFloat(newRadius.Plus(n.Center.Float())),
			newRadius, n.MinRadius)
		return left, right, true
	} else {
		return nil, nil, false
	}
}

func (n NeuronId) String() string {
	return fmt.Sprintf("Center: %s, radius: %d, min radius: %d", n.Center, n.Radius.Int(), n.MinRadius.Int())
}

func (n NeuronId) SetupNeuron(bin gomachine.Bin) {
	if trained, ok := bin.Get("neuron_trained"); !ok {
		bin.Put("neuron_trained", false)
	} else {
		if trained.(bool) == true {
			return
		}
	}
	fmt.Println("Setting up neuron @", n)
	var wn *WaveformNeuron
	left, right, split := n.Split()
	if !split {
		wn = NewWaveformNeuron(1)
	} else {
		bin.Put("left", left)
		bin.Put("right", right)
		wn = NewWaveformNeuron(3)
		leftBin := bin.AddChild(left)
		rightBin := bin.AddChild(right)
		left.SetupNeuron(leftBin)
		right.SetupNeuron(rightBin)
	}
	bin.Put("waveform_neuron", wn)
	bin.Persist()
}

func (n NeuronId) TrainTerminal(bin gomachine.Bin, samples []wav.Sample) {
	rawWn, _ := bin.Get("waveform_neuron")
	wn := rawWn.(*WaveformNeuron)
	for i := 0; i < len(samples); i++ {
		if i == n.Center.Int() {
			wn.AddTrainingEntry(1.0, float64(samples[i]))
		} else {
			wn.AddTrainingEntry(0.0, float64(samples[i]))
		}
	}
	wn.PerformTraining()
	bin.Put("waveform_neuron", wn)
	bin.Put("neuron_trained", true)
}

func (n NeuronId) TrainConnected(bin gomachine.Bin, sw *SampleWindow) {
	rawWn, _ := bin.Get("waveform_neuron")
	wn := rawWn.(*WaveformNeuron)
	samples, found := sw.NextWindow()
	sampleLen := len(samples)
	left, _ := bin.Get("left")
	right, _ := bin.Get("right")
	leftBin := bin.AddChild(left)
	rightBin := bin.AddChild(right)
	for i := 0; i < sampleLen; i++ {
		leftResult := left.(*NeuronId).Evaluate(leftBin, samples)
		rightResult := right.(*NeuronId).Evaluate(rightBin, samples)
		if i == 0 {
			wn.AddTrainingEntry(1.0, leftResult, float64(samples[n.Center.Int()]), rightResult)
		} else {
			wn.AddTrainingEntry(0.0, leftResult, float64(samples[n.Center.Int()]), rightResult)
		}
		samples, found = sw.NextWindow()
		if !found {
			break
		}
	}
	wn.PerformTraining()
	bin.Put("waveform_neuron", wn)
	bin.Put("neuron_trained", true)
}

func (n NeuronId) Train(bin gomachine.Bin, sw *SampleWindow) {
	if rawTrained, ok := bin.Get("neuron_trained"); ok {
		trained := rawTrained.(bool)
		if trained {
			return
		}
	}
	fmt.Println("Training @", n)
	children := bin.Children()
	if bin.Terminal() {
		wavSamples, found := sw.CurrentWindow()
		if !found {
			return
		}
		n.TrainTerminal(bin, wavSamples)
	} else {
		for id, child := range children {
			id.(*NeuronId).Train(child, sw.Clone())
		}
		n.TrainConnected(bin, sw.Clone())
	}
	bin.Persist()
}

func (n NeuronId) Evaluate(bin gomachine.Bin, samples []wav.Sample) float64 {
	rawWn, _ := bin.Get("waveform_neuron")
	wn := rawWn.(*WaveformNeuron)
	if bin.Terminal() {
		return wn.Update(math.Abs(float64(samples[n.Center.Int()])))
	} else {
		left, _ := bin.Get("left")
		right, _ := bin.Get("right")
		leftBin := bin.AddChild(left)
		rightBin := bin.AddChild(right)
		leftResult := left.(*NeuronId).Evaluate(leftBin, samples)
		rightResult := right.(*NeuronId).Evaluate(rightBin, samples)
		return wn.Update(leftResult, float64(samples[n.Center.Int()]), rightResult)
	}
}
