package neuronmachinelib

import (
	"fmt"
	"github.com/unixpickle/wav"
	"gomachine"
	"gomachine/ioutil"
	"time"
)

func SetupTree(m *gomachine.Machine) error {
	m.SetVMImport("neuronmachine", Imports)
	m.DefineVMValue("setup_ear_frequency", func(name string, frequency float64) {
		earRoot := m.AddChild("ear")
		ear := NewEar(name)
		earBin := earRoot.AddChild(ear)
		ear.SetupFrequency(earBin, frequency)
		ear.Train(earBin)
	})
	m.DefineVMValue("test_ear_frequency", func(name string, frequency float64) {
		earRoot := m.AddChild("ear")
		ear := NewEar(name)
		earBin := earRoot.AddChild(ear)
		sr := NewSampleRate(11025, NewFrequency(frequency))
		sound := sr.GenerateSine(10 * time.Second)
		start := time.Now()
		results := ear.Evaluate(earBin, sound)
		elapsed := time.Since(start)
		fmt.Println("Elapsed time:", elapsed.Seconds(), "sec")
		for freq, samples := range results {
			sound.Write(ioutil.CreateFile(fmt.Sprintf("/home/fernando/Test-%f-%d.wav", freq, sr.Frequency.Int())))
			resultSound := wav.NewPCM8Sound(1, sr.SamplePos.Int())
			resultSound.SetSamples(samples)
			resultSound.Write(ioutil.CreateFile(fmt.Sprintf("/home/fernando/Result-%f-%d.wav", freq,
				sr.Frequency.Int())))
		}
	})
	return nil
}
