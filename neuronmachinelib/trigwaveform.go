package neuronmachinelib

import (
	"github.com/unixpickle/wav"
	"math"
	"time"
)

type TrigWaveform struct {
	wav.Sound
	frequency      float64
	samplesPerWave float64
	sampleRate     int
	length         time.Duration
	totalSamples   int
	pctPerSample   float64
}

func NewTrigWaveform(frequency float64, sampleRate int, length time.Duration) *TrigWaveform {
	ret := &TrigWaveform{Sound: wav.NewPCM8Sound(1, sampleRate), frequency: frequency,
		samplesPerWave: float64(sampleRate) / frequency, length: length, sampleRate: sampleRate,
		totalSamples: int(math.Floor(length.Seconds() * float64(sampleRate)))}
	ret.build()
	return ret
}

func (t *TrigWaveform) build() {
	samples := make([]wav.Sample, 0, t.totalSamples)
	for i := 0; i < t.totalSamples; i++ {
		if i == 0 {
			samples = append(samples, wav.Sample(0))
		} else {
			instant := float64(i) / float64(t.sampleRate)
			value := wav.Sample(math.Sin(instant * math.Pi * 2 * t.frequency))
			if value >= 1.0 {
				value = 0.9999999
			} else if value <= -1.0 {
				value = -0.9999999
			}
			samples = append(samples, value)
		}
	}
	t.Sound.SetSamples(samples)
}

func (t *TrigWaveform) Frequency() float64 {
	return t.frequency
}

func (t *TrigWaveform) SampleLen() int {
	return t.totalSamples
}

func (t *TrigWaveform) GetSample(pos int) float64 {
	return float64(t.Sound.Samples()[pos])
}

func (t *TrigWaveform) SetSample(pos int, value float64) {
	t.Sound.Samples()[pos] = wav.Sample(value)
}

func (t *TrigWaveform) Angle(sample int) float64 {
	return math.Mod(float64(sample), t.samplesPerWave) / t.samplesPerWave
}

func (t *TrigWaveform) Clone() *TrigWaveform {
	return &TrigWaveform{Sound: t.Sound.Clone(), frequency: t.frequency, samplesPerWave: t.samplesPerWave,
		length: t.length, totalSamples: t.totalSamples, sampleRate: t.sampleRate}
}
