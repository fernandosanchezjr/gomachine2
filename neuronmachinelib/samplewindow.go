package neuronmachinelib

import (
	"fmt"
	"github.com/unixpickle/wav"
)

type SampleWindow struct {
	Samples    []wav.Sample
	LastSample int
	Width      int
	Start      int
}

func NewSampleWindow(sound wav.Sound, width int) *SampleWindow {
	ret := &SampleWindow{Samples: sound.Samples(), Width: width}
	ret.LastSample = len(ret.Samples) - 1
	return ret
}

func NewSampleWindowFromSamples(samples []wav.Sample, width int) *SampleWindow {
	ret := &SampleWindow{Samples: samples, Width: width}
	ret.LastSample = len(ret.Samples) - 1
	return ret
}

func (sw *SampleWindow) String() string {
	return fmt.Sprint("window of ", sw.Width, " samples @ ", sw.Start)
}

func (sw *SampleWindow) CurrentWindow() ([]wav.Sample, bool) {
	endpos := sw.Start + sw.Width
	if endpos > sw.LastSample {
		return nil, false
	}
	ret := sw.Samples[sw.Start : sw.Start+sw.Width]
	return ret, true
}

func (sw *SampleWindow) NextWindow() ([]wav.Sample, bool) {
	endpos := sw.Start + sw.Width
	if endpos > sw.LastSample {
		return nil, false
	}
	ret := sw.Samples[sw.Start : sw.Start+sw.Width]
	sw.Start = sw.Start + 1
	return ret, true
}

func (sw *SampleWindow) NextWindowOffset(offset int) ([]wav.Sample, bool) {
	endpos := sw.Start + sw.Width
	if endpos > sw.LastSample {
		return nil, false
	}
	ret := sw.Samples[sw.Start : sw.Start+sw.Width]
	sw.Start = sw.Start + offset
	return ret, true
}

func (sw *SampleWindow) Clone() *SampleWindow {
	ret := &SampleWindow{Samples: sw.Samples, Start: sw.Start, Width: sw.Width, LastSample: sw.LastSample}
	return ret
}
