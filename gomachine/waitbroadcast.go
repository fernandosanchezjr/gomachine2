package gomachine

import (
	"sync"
)

type WaitBroadcast struct {
	mtx     sync.Mutex
	waiters []chan int
	busy    bool
}

func NewWaitBroadcast() *WaitBroadcast {
	bnb := &WaitBroadcast{waiters: make([]chan int, 0)}
	return bnb
}

func (bnb *WaitBroadcast) GetWaiter() chan int {
	bnb.mtx.Lock()
	defer bnb.mtx.Unlock()
	c := make(chan int)
	bnb.waiters = append(bnb.waiters, c)
	return c
}

func (bnb *WaitBroadcast) Busy() bool {
	return bnb.busy
}

func (bnb *WaitBroadcast) MarkBusy() {
	if bnb.busy {
		return
	}
	bnb.mtx.Lock()
	defer bnb.mtx.Unlock()
	bnb.busy = true
}

func (bnb *WaitBroadcast) UnmarkBusy() {
	bnb.mtx.Lock()
	defer bnb.mtx.Unlock()
	if !bnb.busy {
		return
	}
	bnb.busy = false
	for _, c := range bnb.waiters {
		close(c)
	}
	bnb.waiters = make([]chan int, 0)
}

func (bnb *WaitBroadcast) Wait() {
	if !bnb.Busy() {
		return
	}
	w := bnb.GetWaiter()
	select {
	case <-w:
		return
	}
}
