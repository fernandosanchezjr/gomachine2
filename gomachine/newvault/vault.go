package newvault

import (
	"bytes"
	"fmt"
	"github.com/boltdb/bolt"
	"gomachine/address"
	"gomachine/ioutil"
	"gomachine/operations"
	"path"
	"sync"
	"time"
)

type Vault struct {
	rootPath     string
	count        *operations.Count
	waiter       *operations.Wait
	syncDuration time.Duration
	connsMtx     sync.Mutex
	conns        map[byte]*bolt.DB
	connSyncs    map[byte]chan int
}

func NewVault(rootPath string, syncDuration time.Duration) *Vault {
	v := &Vault{rootPath: rootPath, waiter: operations.NewWait(), syncDuration: syncDuration,
		conns: make(map[byte]*bolt.DB), connSyncs: make(map[byte]chan int)}
	v.count = operations.NewCount(v.onOperationsComplete)
	v.init()
	//go v.syncLoop()
	return v
}

func (v *Vault) init() {
	v.createFolder()
	//v.createConnection()
	fmt.Println("Started vault at", v.rootPath)
}

func (v *Vault) createFolder() {
	ioutil.CreatePath(v.rootPath)
}

func (v *Vault) createConnection(id byte) (*bolt.DB, chan int) {
	v.connsMtx.Lock()
	defer v.connsMtx.Unlock()
	if conn, ok := v.conns[id]; ok {
		return conn, v.connSyncs[id]
	}
	if conn, err := bolt.Open(path.Join(v.rootPath, fmt.Sprintf("%02x.db", id)), 0660,
		&bolt.Options{NoGrowSync: true}); err != nil {
		panic(err)
	} else {
		v.conns[id] = conn
		sync := make(chan int)
		v.connSyncs[id] = sync
		go v.syncLoop(conn, sync)
		return conn, sync
	}
}

func (v *Vault) StartOperation() {
	v.count.StartOperation()
}

func (v *Vault) OperationComplete() {
	v.count.OperationComplete()
}

func (v *Vault) onOperationsComplete() {
	v.waiter.Notify()
}

func (v *Vault) Busy() bool {
	return v.count.Busy()
}

func (v *Vault) Wait() {
	if !v.Busy() {
		return
	}
	v.waiter.Wait()
}

func (v *Vault) doSync(c chan int) {
	c <- 1
}

func (v *Vault) Write(did address.Did, key []byte, data *bytes.Buffer) error {
	v.StartOperation()
	defer v.OperationComplete()
	hash := did.Hash()
	wo := NewVaultWrite(hash, key, data)
	conn, sync := v.createConnection(hash[0])
	defer v.doSync(sync)
	return wo.ExecuteBoltOperation(conn)
}

func (v *Vault) Delete(did address.Did) error {
	v.StartOperation()
	defer v.OperationComplete()
	hash := did.Hash()
	do := NewVaultDelete(hash)
	conn, sync := v.createConnection(hash[0])
	defer v.doSync(sync)
	return do.ExecuteBoltOperation(conn)
}

func (v *Vault) Read(did address.Did, key []byte) (*bytes.Buffer, error) {
	v.StartOperation()
	defer v.OperationComplete()
	hash := did.Hash()
	ro := NewVaultRead(hash, key)
	conn, sync := v.createConnection(hash[0])
	defer v.doSync(sync)
	if err := ro.ExecuteBoltOperation(conn); err != nil {
		return nil, err
	}
	return ro.RawBytes, nil
}

func (v *Vault) Close() error {
	v.Wait()
	v.connsMtx.Lock()
	defer v.connsMtx.Unlock()
	for _, c := range v.connSyncs {
		c <- 0
	}
	for _, c := range v.conns {
		if err := c.Close(); err != nil {
			return err
		}
	}
	return nil
	//return v.conn.Close()
}

func (v *Vault) syncLoop(conn *bolt.DB, syncChan chan int) {
	sync := false
	for {
		select {
		case s := <-syncChan:
			switch s {
			case 0:
				return
			default:
				sync = true
			}
		case <-time.After(v.syncDuration):
			if sync {
				conn.Sync()
				sync = false
			}
		}
	}
}
