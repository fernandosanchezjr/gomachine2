package newvault

import (
	"bytes"
	"github.com/boltdb/bolt"
)

type VaultWrite struct {
	Bucket []byte
	Key    []byte
	Data   *bytes.Buffer
}

func NewVaultWrite(bucket []byte, key []byte, data *bytes.Buffer) *VaultWrite {
	vw := &VaultWrite{Bucket: bucket, Key: key, Data: data}
	return vw
}

func (vw *VaultWrite) ExecuteBoltOperation(conn *bolt.DB) error {
	return conn.Update(vw.performWrite)
}

func (vw *VaultWrite) performWrite(tx *bolt.Tx) error {
	bucket, err := tx.CreateBucketIfNotExists(vw.Bucket)
	if err != nil {
		return err
	}
	return bucket.Put(vw.Key, vw.Data.Bytes())
}
