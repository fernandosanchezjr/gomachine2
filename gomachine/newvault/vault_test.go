package newvault

import (
	"bytes"
	"fmt"
	"gomachine/address"
	"gomachine/ioutil"
	"gomachine/strategies"
	"sync"
	"testing"
	"time"
)

const iterations int = 1000000

var benchMarkId int = 0
var benchMarkMtx sync.Mutex

func writeTest(t *testing.T, v *Vault, did address.Did) {
	if err := v.Write(did, []byte("test"), bytes.NewBuffer(did.Hash())); err != nil {
		t.Fatal("Write error:", err)
	}
}

func readTest(t *testing.T, v *Vault, did address.Did, i int) {
	buf, err := v.Read(did, []byte("test"))
	if err != nil {
		t.Fatal("Read error:", err)
	}
	if buf == nil {
		t.Fatal("Failed empty read from", i)
	}
	if buf.Len() == 0 {
		t.Fatal("Failed read from", i)
	}
}

func TestVaultWrites(t *testing.T) {
	ioutil.DeletePath("/opt/vault/TestVaultWrites")
	v := NewVault("/opt/vault/TestVaultWrites", time.Duration(100)*time.Millisecond)
	fmt.Println("Started write test")
	start := time.Now()
	for i := 0; i < iterations; i++ {
		did := strategies.Number(i)
		go writeTest(t, v, did)
	}
	fmt.Println("Completed writes", time.Since(start))
	v.Wait()
	fmt.Println("Completed test", time.Since(start))
	v.Close()
}

func TestVaultReadWrites(t *testing.T) {
	ioutil.DeletePath("/opt/vault/TestVaultReadWrites")
	v := NewVault("/opt/vault/TestVaultReadWrites", time.Duration(100)*time.Millisecond)
	fmt.Println("Started write test")
	start := time.Now()
	for i := 0; i < iterations; i++ {
		did := strategies.Number(i)
		go writeTest(t, v, did)
	}
	v.Wait()
	fmt.Println("Completed writes", time.Since(start))
	for i := 0; i < iterations; i++ {
		did := strategies.Number(i)
		go readTest(t, v, did, i)
	}
	fmt.Println("Completed reads", time.Since(start))
	v.Wait()
	fmt.Println("Completed test", time.Since(start))
	v.Close()
}

func BenchmarkVaultWrites(b *testing.B) {
	benchMarkMtx.Lock()
	benchMarkId += 1
	currId := benchMarkId
	benchMarkMtx.Unlock()
	path := fmt.Sprintf("/opt/vault/BenchmarkVaultWrites-%d", currId)
	ioutil.DeletePath(path)
	v := NewVault(path, time.Duration(100)*time.Millisecond)
	fmt.Println("Started write test")
	start := time.Now()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		did := strategies.Number(i)
		go v.Write(did, []byte("test"), bytes.NewBuffer(did.Hash()))
	}
	b.StopTimer()
	fmt.Println("Completed writes", time.Since(start))
	v.Wait()
	fmt.Println("Completed test", time.Since(start))
	v.Close()
}
