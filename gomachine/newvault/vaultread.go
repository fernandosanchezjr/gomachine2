package newvault

import (
	"bytes"
	"github.com/boltdb/bolt"
)

type VaultRead struct {
	Bucket   []byte
	Key      []byte
	RawBytes *bytes.Buffer
}

func NewVaultRead(bucket []byte, key []byte) *VaultRead {
	vw := &VaultRead{Bucket: bucket, Key: key}
	return vw
}

func (vr *VaultRead) ExecuteBoltOperation(conn *bolt.DB) error {
	return conn.Update(vr.performRawRead)
}

func (vr *VaultRead) performRawRead(tx *bolt.Tx) error {
	bucket, err := tx.CreateBucketIfNotExists(vr.Bucket)
	if err != nil {
		return err
	}
	if data := bucket.Get(vr.Key); data != nil {
		vr.RawBytes = bytes.NewBuffer(data)
	}
	return nil
}
