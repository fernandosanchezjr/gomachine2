package newvault

import (
	"github.com/boltdb/bolt"
)

type VaultDelete struct {
	Bucket []byte
}

func NewVaultDelete(bucket []byte) *VaultDelete {
	vw := &VaultDelete{Bucket: bucket}
	return vw
}
func (vd *VaultDelete) ExecuteBoltOperation(conn *bolt.DB) error {
	return conn.Update(vd.performDelete)
}

func (vd *VaultDelete) performDelete(tx *bolt.Tx) error {
	return tx.DeleteBucket(vd.Bucket)
}
