package gomachine

import (
	"bytes"
	"fmt"
	"github.com/kr/pty"
	"os"
	"os/exec"
)

type InteractiveProcessController struct {
	cmd      *exec.Cmd
	pty      *os.File
	exited   bool
	waitChan chan error
}

func NewInteractiveProcessController(name string, args ...string) (*InteractiveProcessController, error) {
	ipc := &InteractiveProcessController{cmd: exec.Command(name, args...), waitChan: make(chan error)}
	if err := ipc.initialize(); err != nil {
		return nil, err
	}
	return ipc, nil
}

func (ipc *InteractiveProcessController) initialize() error {
	var err error
	ipc.pty, err = pty.Start(ipc.cmd)
	go func() {
		err := ipc.cmd.Wait()
		ipc.exited = true
		ipc.waitChan <- err
	}()
	return err
}

func (ipc *InteractiveProcessController) ReadAll() string {
	var buffer bytes.Buffer
	var data []byte = make([]byte, 1024)
	for {
		count, err := ipc.pty.Read(data)
		buffer.Write(data[:count])
		if err != nil {
			break
		}
	}
	return buffer.String()
}

func (ipc *InteractiveProcessController) Read() string {
	var data []byte = make([]byte, 1024)
	count, _ := ipc.pty.Read(data)
	return string(data[:count])
}

func (ipc *InteractiveProcessController) Write(data string) {
	_, err := ipc.pty.Write([]byte(data))
	if err != nil {
		fmt.Println("Write error:", err)
	}
}

func (ipc *InteractiveProcessController) Release() error {
	return ipc.cmd.Process.Release()
}

func (ipc *InteractiveProcessController) Kill() error {
	return ipc.cmd.Process.Kill()
}

func (ipc *InteractiveProcessController) Wait() chan error {
	return ipc.waitChan
}

func (ipc *InteractiveProcessController) Exited() bool {
	return ipc.exited
}

func (ipc *InteractiveProcessController) Running() bool {
	return !ipc.Exited()
}
