package gomachine

import (
	"bytes"
	"fmt"
	"gomachine/address"
	"gomachine/operations"
	"reflect"
	"sync"
)

var binDataKey []byte = []byte("binData")

type baseBin struct {
	count       *operations.Count
	m           *Machine
	did         address.Did
	didHash     []byte
	didPath     string
	binName     string
	parent      address.Did
	changed     bool
	isDisowned  bool
	childCount  int
	dataCount   int
	metaCount   int
	beatsCount  int
	children    map[string]address.Did
	data        map[string]interface{}
	meta        map[string]interface{}
	beats       map[string]*VMCode
	mtx         sync.Mutex
	externalMtx sync.Mutex
	persisting  bool
}

func newChildBin(did address.Did, parent address.Did, machine *Machine) *baseBin {
	newBin := &baseBin{did: did, parent: parent, m: machine, children: make(map[string]address.Did),
		data: make(map[string]interface{}), meta: make(map[string]interface{}),
		beats: make(map[string]*VMCode)}
	newBin.count = operations.NewCount(newBin.OnOperationsComplete)
	newBin.init()
	return newBin
}

func (b *baseBin) init() {
	b.mtx.Lock()
	defer b.mtx.Unlock()
	b.loadData()
}

func (b *baseBin) getVM() *ankoVM {
	vm := b.m.newChildVM()
	return vm
}

func (b *baseBin) Lock() {
	b.externalMtx.Lock()
	b.StartOperation()
}

func (b *baseBin) Unlock() {
	b.externalMtx.Unlock()
	b.OperationComplete()
}

func (b *baseBin) internalLock() {
	b.StartOperation()
	b.mtx.Lock()
}

func (b *baseBin) internalUnlock() {
	b.mtx.Unlock()
	b.OperationComplete()
}

func (b *baseBin) String() string {
	if b.binName == "" {
		if b.m != nil {
			b.binName = fmt.Sprintf("%s:%s", b.m.Name(), b.Did().String())
		} else {
			b.binName = fmt.Sprintf(b.Did().String())
		}
	}
	return b.binName
}

func (b *baseBin) Machine() *Machine {
	return b.m
}

func (b *baseBin) Did() address.Did {
	return b.did
}

func (b *baseBin) Hash() []byte {
	if b.didHash == nil {
		b.didHash = b.Did().Hash()
	}
	return b.didHash
}

func (b *baseBin) Path() string {
	if b.didPath == "" {
		b.didPath = b.Did().Path()
	}
	return b.didPath
}

func (b *baseBin) Terminal() bool {
	return b.ChildCount() == 0
}

func (b *baseBin) Id() interface{} {
	return b.did[len(b.did)-1]
}

func (b *baseBin) Root() bool {
	return b.did.Root()
}

func (b *baseBin) Parent() Bin {
	if b.Root() {
		return b.m
	}
	parent := b.m.loadBin(b.parent)
	return parent
}

func (b *baseBin) disowned() bool {
	return b.isDisowned
}

func (b *baseBin) disown() {
	if b.Root() {
		return
	}
	b.internalLock()
	defer b.internalUnlock()
	b.changed = true
	b.isDisowned = true
}

func (b *baseBin) Children() map[interface{}]Bin {
	b.internalLock()
	defer b.internalUnlock()
	retMap := make(map[interface{}]Bin)
	for _, did := range b.children {
		retMap[did.Id()] = b.m.loadBin(did)
	}
	return retMap
}

func (b *baseBin) ChildIds() []interface{} {
	b.internalLock()
	defer b.internalUnlock()
	retList := make([]interface{}, len(b.children))
	pos := 0
	for _, did := range b.children {
		retList[pos] = did.Id()
		pos = pos + 1
	}
	return retList
}

func (b *baseBin) ChildCount() int {
	b.internalLock()
	defer b.internalUnlock()
	return b.childCount
}

func (b *baseBin) AddChild(id interface{}) Bin {
	b.internalLock()
	defer b.internalUnlock()
	var childDid address.Did
	var found bool
	sid := address.ToString(id)
	if childDid, found = b.children[sid]; !found {
		childDid = b.Did().Append(id)
	}
	child, _ := b.m.lookupOrCreateBin(childDid, b.childCreator)
	if bBin, ok := child.(*baseBin); ok == true && found == false {
		bBin.copyBeats(b)
		bBin.changed = true
		b.children[sid] = childDid
		b.childCount = len(b.children)
		b.changed = true
	}
	return child
}

func (b *baseBin) killChild(id interface{}) {
	childDid := b.Did().Append(id)
	bin := b.m.loadBin(childDid)
	bin.disown()
}

func (b *baseBin) RemoveChild(id interface{}) {
	b.internalLock()
	defer b.internalUnlock()
	sid := address.ToString(id)
	if _, ok := b.children[sid]; ok {
		b.changed = true
		delete(b.children, sid)
		b.childCount = len(b.children)
		go b.killChild(id)
	}
}

func (b *baseBin) copyBeats(bin *baseBin) {
	b.internalLock()
	defer b.internalUnlock()
	for name, beat := range bin.beats {
		b.beats[name] = beat
	}
	b.beatsCount = len(b.beats)
	b.changed = true
}

func (b *baseBin) childCreator(did address.Did) Bin {
	bin := newChildBin(did, b.Did(), b.m)
	return bin
}

func (b *baseBin) GetChild(id interface{}) (Bin, bool) {
	b.internalLock()
	defer b.internalUnlock()
	var childDid address.Did
	var found bool
	sid := address.ToString(id)
	if childDid, found = b.children[sid]; !found {
		childDid = b.Did().Append(id)
	}
	bin, _ := b.m.lookupOrCreateBin(childDid, b.childCreator)
	if bBin, ok := bin.(*baseBin); ok == true && found == false {
		bBin.copyBeats(b)
		bBin.changed = true
		b.children[sid] = childDid
		b.childCount = len(b.children)
		b.changed = true
		if creationBeats {
			go b.Beat("new_child", bBin)
			go bBin.Beat("creation")
		}
	}
	return bin, !found
}

func (b *baseBin) Put(key string, value interface{}) {
	b.internalLock()
	defer b.internalUnlock()
	b.changed = true
	b.data[key] = value
	b.dataCount = len(b.data)
}

func (b *baseBin) GetKeys() []string {
	b.internalLock()
	defer b.internalUnlock()
	keys := make([]string, b.dataCount)
	pos := 0
	for key := range b.data {
		keys[pos] = key
		pos += 1
	}
	return keys
}

func (b *baseBin) Get(key string) (interface{}, bool) {
	b.internalLock()
	defer b.internalUnlock()
	value, ok := b.data[key]
	return value, ok
}

func (b *baseBin) Delete(key string) {
	b.internalLock()
	defer b.internalUnlock()
	if _, ok := b.data[key]; ok {
		b.changed = true
		delete(b.data, key)
		b.dataCount = len(b.data)
	}
}

func (b *baseBin) ClearData() {
	b.internalLock()
	defer b.internalUnlock()
	b.data = make(map[string]interface{})
	b.changed = true
}

func (b *baseBin) PutMeta(key string, value interface{}) {
	b.internalLock()
	defer b.internalUnlock()
	b.changed = true
	b.meta[key] = value
	b.metaCount = len(b.meta)
}

func (b *baseBin) GetMetaKeys() []string {
	b.internalLock()
	defer b.internalUnlock()
	keys := make([]string, b.metaCount)
	pos := 0
	for key := range b.meta {
		keys[pos] = key
		pos += 1
	}
	return keys
}

func (b *baseBin) GetMeta(key string) (interface{}, bool) {
	b.internalLock()
	defer b.internalUnlock()
	value, ok := b.meta[key]
	return value, ok
}

func (b *baseBin) DeleteMeta(key string) {
	b.internalLock()
	defer b.internalUnlock()
	if _, ok := b.meta[key]; ok {
		b.changed = true
		delete(b.meta, key)
		b.metaCount = len(b.meta)
	}
}

func (b *baseBin) ClearMeta() {
	b.internalLock()
	defer b.internalUnlock()
	b.meta = make(map[string]interface{})
}

func (b *baseBin) PutBeat(key string, beat string) error {
	b.internalLock()
	defer b.internalUnlock()
	vm := b.getVM()
	if vmcode, _, err := stringToVMCodeFunc(vm, beat); err != nil {
		return err
	} else {
		b.beats[key] = vmcode
		b.beatsCount = len(b.beats)
		b.changed = true
	}
	return nil
}

func (b *baseBin) GetBeat(key string) (*VMCode, bool) {
	b.internalLock()
	defer b.internalUnlock()
	beat, found := b.beats[key]
	if !b.disowned() && !b.Root() && !found {
		return b.Parent().GetBeat(key)
	}
	return beat, found
}

func (b *baseBin) DeleteBeat(key string) {
	b.internalLock()
	defer b.internalUnlock()
	if _, found := b.beats[key]; !found {
		return
	} else {
		delete(b.beats, key)
		b.beatsCount = len(b.beats)
		b.changed = true
	}
}

func (b *baseBin) ClearBeats() {
	b.internalLock()
	defer b.internalUnlock()
	b.beats = make(map[string]*VMCode)
	b.changed = true
}

func (b *baseBin) GetBeatNames() []string {
	b.internalLock()
	defer b.internalUnlock()
	ret := make([]string, b.beatsCount)
	pos := 0
	for key, _ := range b.beats {
		ret[pos] = key
		pos += 1
	}
	return ret
}

func (b *baseBin) StartOperation() {
	b.count.StartOperation()
}

func (b *baseBin) OperationComplete() {
	b.count.OperationComplete()
}

func (b *baseBin) OnOperationsComplete() {
	b.mtx.Lock()
	defer b.mtx.Unlock()
	switch {
	case b.disowned():
		b.Unpersist()
	case b.Changed():
		b.internalPersist()
	}
}

func (b *baseBin) Changed() bool {
	return b.changed
}

func (b *baseBin) Busy() bool {
	return b.count.Busy()
}

func (b *baseBin) Empty() bool {
	b.mtx.Lock()
	defer b.mtx.Unlock()
	return b.childCount == 0 && b.dataCount == 0 && b.metaCount == 0 && b.beatsCount == 0
}

func (b *baseBin) serialize(buf *bytes.Buffer) error {
	bd := newBinData(b)
	return bd.serialize(buf)
}

func (b *baseBin) deserialize(buf *bytes.Buffer) error {
	bd := &binData{}
	if err := bd.deserialize(buf); err != nil {
		return err
	}
	b.childCount = bd.ChildCount
	b.dataCount = bd.DataCount
	b.metaCount = bd.MetaCount
	b.beatsCount = bd.BeatsCount
	b.data = bd.Data
	b.meta = bd.Meta
	b.beats = bd.Beats
	b.children = bd.Children
	return nil
}

func (b *baseBin) loadData() {
	if buf, err := b.m.vault.Read(b.did, binDataKey); err != nil {
		fmt.Println("Error deserializing", b, "-", err)
	} else if buf != nil {
		if err := b.deserialize(buf); err != nil {
			fmt.Println("Error deserializing", b, "-", err)
		}
	}
}

func (b *baseBin) internalPersist() {
	var buf bytes.Buffer
	if err := b.serialize(&buf); err != nil {
		fmt.Println("Error serializing", b, "-", err)
	} else {
		if err := b.m.vault.Write(b.did, binDataKey, &buf); err != nil {
			fmt.Println("Error writing", b, "-", err)
		}
		b.changed = false
	}
}

func (b *baseBin) Persist() {
	b.internalLock()
	defer b.internalUnlock()
	b.internalPersist()
}

func (b *baseBin) Unpersist() {
	b.m.vault.Delete(b.did)
}

func (b *baseBin) recoverExecute(source string) {
	if err := recover(); err != nil {
		fmt.Printf("Caught error running %s: %s", source, err)
	}
}

func (b *baseBin) Execute(source string) (interface{}, error) {
	b.Lock()
	defer b.Unlock()
	vm := b.getVM()
	defer vm.Destroy()
	nb := newCurrentBin(b)
	vm.Define("bin", (Bin)(b))
	defer nb.cleanup()
	defer b.recoverExecute(source)
	return vm.Execute(source)
}

func (b *baseBin) toVMCodeValueArgs(nb *currentBin, args []interface{}) []reflect.Value {
	ret := make([]reflect.Value, len(args)+1)
	ret[0] = reflect.ValueOf((Bin)(nb))
	for pos, arg := range args {
		ret[pos+1] = reflect.ValueOf(arg)
	}
	return ret
}

func (b *baseBin) recoverBeat(name string, args []interface{}) {
	if err := recover(); err != nil {
		fmt.Printf("Caught error running beat %s(%#v): %s", name, args, err)
	}
}

func (b *baseBin) Beat(name string, args ...interface{}) (interface{}, error) {
	b.Lock()
	defer b.Unlock()
	beat, found := b.GetBeat(name)
	if !found {
		return nil, nil
	}
	if beat == nil {
		return nil, nil
	}
	vm := b.getVM()
	defer vm.Destroy()
	nb := newCurrentBin(b)
	defer nb.cleanup()
	defer b.recoverBeat(name, args)
	beatFunc, err := beat.InterpretFunction(vm)
	if err != nil {
		return nil, err
	}
	if result, err := beatFunc(b.toVMCodeValueArgs(nb, args)...); err != nil {
		return nil, err
	} else {
		return result.Interface(), nil
	}
}

func (b *baseBin) ContextBeat(name string, context map[string]interface{}, args ...interface{}) (interface{}, error) {
	b.Lock()
	defer b.Unlock()
	beat, found := b.GetBeat(name)
	if !found {
		return nil, nil
	}
	if beat == nil {
		return nil, nil
	}
	vm := b.getVM()
	for k, v := range context {
		vm.Define(k, v)
	}
	defer vm.Destroy()
	nb := newCurrentBin(b)
	defer nb.cleanup()
	defer b.recoverBeat(name, args)
	beatFunc, err := beat.InterpretFunction(vm)
	if err != nil {
		return nil, err
	}
	if result, err := beatFunc(b.toVMCodeValueArgs(nb, args)...); err != nil {
		return nil, err
	} else {
		return result.Interface(), nil
	}
}

func (b *baseBin) Broadcast(bcast Broadcast) {
	b.StartOperation()
	bcast.StartOperation()
	go bcast.broadcastStep(b)
}

func (b *baseBin) Clear() {
	b.ClearData()
	b.ClearMeta()
	b.ClearBeats()
}
