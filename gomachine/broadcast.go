package gomachine

import (
	"fmt"
	"gomachine/address"
	"gomachine/operations"
)

const UnlimitedBroadcast int = -1

type baseBroadcast struct {
	startDid      address.Did
	depth         int
	stopCheckFunc func(Broadcast, Bin) bool
	executeFunc   func(b Bin) interface{}
	m             *Machine
	count         *operations.Count
	waiter        *operations.Wait
}

func newBroadcast(did address.Did, m *Machine, depth int, stopCheck func(Broadcast, Bin) bool,
	broadcastFunc func(b Bin) interface{}, completer operations.OperationsCompleteMonitor) Broadcast {
	b := &baseBroadcast{startDid: did, depth: depth, stopCheckFunc: stopCheck, executeFunc: broadcastFunc,
		m: m, waiter: operations.NewWait()}
	if completer != nil {
		b.count = operations.NewCount(b.completerWrapper(completer.OnOperationsComplete))
	} else {
		b.count = operations.NewCount(b.OnOperationsComplete)
	}
	return b
}

func (b *baseBroadcast) completerWrapper(f func()) func() {
	return func() {
		f()
		b.OnOperationsComplete()
	}
}

func (b *baseBroadcast) Did() address.Did {
	return b.startDid
}

func (b *baseBroadcast) String() string {
	return fmt.Sprint("Broadcast to ", b.startDid.String())
}

func (b *baseBroadcast) Machine() *Machine {
	return b.m
}

func (b *baseBroadcast) Depth() int {
	return b.depth
}

func (b *baseBroadcast) SetDepth(depth int) {
	b.depth = depth
}

func (b *baseBroadcast) stopCheckRecover(bin Bin) {
	if err := recover(); err != nil {
		fmt.Println(b, "caught error on stop check at", bin, " - ", err)
	}
}

func (b *baseBroadcast) stopCheck(bin Bin) bool {
	if b.stopCheckFunc == nil {
		return false
	}
	defer b.stopCheckRecover(bin)
	bin.Lock()
	defer bin.Unlock()
	return (b.stopCheckFunc)(b, bin)
}

func (b *baseBroadcast) stop(bin Bin) bool {
	if b.stopCheck(bin) {
		return true
	}
	depth := b.Depth()
	switch depth {
	case UnlimitedBroadcast:
		return false
	case 0:
		return true
	default:
		return false
	}
}

func (b *baseBroadcast) funcRecover(bin Bin) {
	if err := recover(); err != nil {
		fmt.Println(b, "caught error on stop check at", bin, " - ", err)
	}
}

func (b *baseBroadcast) execute(bin Bin) {
	if b.executeFunc == nil {
		return
	}
	defer b.funcRecover(bin)
	bin.Lock()
	defer bin.Unlock()
	(b.executeFunc)(bin)
}

func (b *baseBroadcast) clone() Broadcast {
	return &baseBroadcast{startDid: b.startDid, depth: b.depth, stopCheckFunc: b.stopCheckFunc,
		executeFunc: b.executeFunc, waiter: b.waiter, count: b.count}
}

func (b *baseBroadcast) spawn() Broadcast {
	depth := b.Depth()
	if depth <= 1 && depth != UnlimitedBroadcast {
		return nil
	}
	clone := b.clone()
	if depth != UnlimitedBroadcast {
		clone.SetDepth(depth - 1)
	}
	return clone
}

func (b *baseBroadcast) Busy() bool {
	return b.count.Busy()
}

func (b *baseBroadcast) Wait() {
	if b.count.Busy() {
		b.waiter.Wait()
	}
}

func (b *baseBroadcast) StartOperation() {
	b.count.StartOperation()
}

func (b *baseBroadcast) OperationComplete() {
	b.count.OperationComplete()
}

func (b *baseBroadcast) OnOperationsComplete() {
	b.waiter.Notify()
}

func (b *baseBroadcast) Start() error {
	return b.m.directBroadcast(b)
}

func (b *baseBroadcast) broadcastStep(bin Bin) {
	defer b.OperationComplete()
	defer bin.OperationComplete()
	if b.stop(bin) {
		return
	}
	b.execute(bin)
	if spawned := b.spawn(); spawned != nil {
		for _, child := range bin.Children() {
			child.Broadcast(spawned.clone())
		}
	}
}
