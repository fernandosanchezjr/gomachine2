package gomachine

import (
	"gomachine/ioutil"
	"path"
)

func createTestMachine(name string) *Machine {
	ioutil.DeletePath(path.Join(machineRoot, name))
	return NewMachine(name)
}

func stringArrayContains(value string, array []string) (bool, int) {
	for pos, v := range array {
		if value == v {
			return true, pos
		}
	}
	return false, -1
}
