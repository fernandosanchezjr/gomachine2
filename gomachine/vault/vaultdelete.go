package vault

import (
	"github.com/boltdb/bolt"
)

type VaultDelete struct {
	Address *VaultAddress
	Vault   *Vault
}

func NewVaultDelete(vault *Vault, address *VaultAddress) *VaultDelete {
	vw := &VaultDelete{Vault: vault, Address: address}
	return vw
}

func (vd *VaultDelete) PoolExecute() {
	conn := vd.Vault.getConnection(vd.Address)
	conn.Update(vd.performDelete)
	vd.Vault.processPool.JobDone()
}

func (vd *VaultDelete) performDelete(tx *bolt.Tx) error {
	tx.DeleteBucket(vd.Address.Bucket)
	return nil
}
