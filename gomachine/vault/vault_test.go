package vault

import (
	"bytes"
	"fmt"
	"gomachine/ioutil"
	"gomachine/strategies"
	"testing"
	"time"
)

func TestVaultAddress(t *testing.T) {
	v := NewVault("/opt/vault/TestVaultAddress", 1024, time.Duration(200)*time.Millisecond)
	ioutil.DeletePath(v.rootPath)
	fmt.Println("Started address test")
	start := time.Now()
	for i := 0; i < 10000; i++ {
		did := strategies.Number(i)
		va := v.NewAddress(did.Hash())
		v.getConnection(va)
	}
	fmt.Println("Completed addressing", time.Since(start))
	v.Wait()
	v.Stop()
	fmt.Println("Completed test", time.Since(start))
}

func TestVaultWrites(t *testing.T) {
	v := NewVault("/opt/vault/TestVaultWrites", 1024, time.Duration(200)*time.Millisecond)
	ioutil.DeletePath(v.rootPath)
	fmt.Println("Started write test")
	start := time.Now()
	for i := 0; i < 10000; i++ {
		did := strategies.Number(i)
		v.Write(did, []byte("test"), bytes.NewBuffer(did.Hash()))
	}
	fmt.Println("Completed writes", time.Since(start))
	v.Wait()
	v.Stop()
	fmt.Println("Completed test", time.Since(start))
}
