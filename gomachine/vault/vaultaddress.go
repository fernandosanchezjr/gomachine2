package vault

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gomachine/ioutil"
	"path"
)

type VaultAddress struct {
	Id         uint32
	FolderPath string
	FilePath   string
	Bucket     []byte
}

func NewVaultAddress(rootPath string, didHash []byte) *VaultAddress {
	reader := bytes.NewBuffer(didHash)
	va := &VaultAddress{FolderPath: path.Join(rootPath, fmt.Sprintf("%x/%x/%x/", didHash[0], didHash[1], didHash[2])),
		Bucket: didHash}
	binary.Read(reader, binary.BigEndian, &va.Id)
	va.FilePath = path.Join(va.FolderPath, fmt.Sprintf("%x.vault", didHash[3]))
	return va
}

func (va *VaultAddress) createFolder() {
	ioutil.CreatePath(va.FolderPath)
}

func (va *VaultAddress) folderExists() bool {
	return ioutil.PathExists(va.FolderPath)
}

func (va *VaultAddress) fileExists() bool {
	return ioutil.PathExists(va.FilePath)
}

func (va *VaultAddress) String() string {
	return va.FilePath
}
