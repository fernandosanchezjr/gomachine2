package vault

import (
	"bytes"
	"fmt"
	"github.com/boltdb/bolt"
)

type VaultRead struct {
	Address *VaultAddress
	Key     []byte
	Vault   *Vault
	Return  chan *bytes.Buffer
}

func NewVaultRead(vault *Vault, address *VaultAddress, key []byte) *VaultRead {
	vw := &VaultRead{Vault: vault, Address: address, Key: key, Return: make(chan *bytes.Buffer)}
	return vw
}

func (vr *VaultRead) PoolExecute() {
	conn := vr.Vault.getConnection(vr.Address)
	if err := conn.View(vr.performRead); err != nil {
		fmt.Println("Read error from", string(vr.Key), vr.Address, err)
	}
	vr.Vault.processPool.JobDone()
}

func (vr *VaultRead) performRead(tx *bolt.Tx) error {
	bucket := tx.Bucket(vr.Address.Bucket)
	if bucket == nil {
		close(vr.Return)
		return nil
	}
	if data := bucket.Get(vr.Key); data == nil {
		vr.Return <- nil
	} else {
		vr.Return <- bytes.NewBuffer(data)
	}
	close(vr.Return)
	return nil
}
