package vault

import (
	"bytes"
	"fmt"
	"github.com/boltdb/bolt"
)

type VaultWrite struct {
	Address *VaultAddress
	Key     []byte
	Data    *bytes.Buffer
	Vault   *Vault
}

func NewVaultWrite(vault *Vault, address *VaultAddress, key []byte, data *bytes.Buffer) *VaultWrite {
	vw := &VaultWrite{Vault: vault, Address: address, Key: key, Data: data}
	return vw
}

func (vw *VaultWrite) PoolExecute() {
	conn := vw.Vault.getConnection(vw.Address)
	if err := conn.Update(vw.performWrite); err != nil {
		fmt.Println("Write error to", string(vw.Key), vw.Address)
	}
	vw.Vault.processPool.JobDone()
}

func (vw *VaultWrite) performWrite(tx *bolt.Tx) error {
	bucket, err := tx.CreateBucketIfNotExists(vw.Address.Bucket)
	if err != nil {
		return err
	}
	return bucket.Put(vw.Key, vw.Data.Bytes())
}
