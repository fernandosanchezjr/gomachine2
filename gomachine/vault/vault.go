package vault

import (
	"bytes"
	"fmt"
	"github.com/ivpusic/grpool"
	"gomachine/address"
	"gomachine/ioutil"
	"runtime"
	"sync"
	"time"
)

type Vault struct {
	mtx         sync.Mutex
	live        map[uint32]*VaultConnection
	maxLive     int
	rootPath    string
	processPool *grpool.Pool
	running     bool
	reapChan    chan int
	reapTimeout time.Duration
}

func NewVault(rootPath string, maxLive int, reapTimeout time.Duration) *Vault {
	v := &Vault{live: make(map[uint32]*VaultConnection), maxLive: maxLive, rootPath: rootPath,
		processPool: grpool.NewPool(maxLive, maxLive*4), reapChan: make(chan int), reapTimeout: reapTimeout}
	v.init()
	return v
}

func (v *Vault) init() {
	v.createFolder()
	v.running = true
	fmt.Println("Started vault at", v.rootPath)
}

func (v *Vault) createFolder() {
	ioutil.CreatePath(v.rootPath)
}

func (v *Vault) NewAddress(didHash []byte) *VaultAddress {
	return NewVaultAddress(v.rootPath, didHash)
}

func (v *Vault) Wait() {
	v.processPool.WaitAll()
}

func (v *Vault) Stop() {
	v.internalLock()
	if !v.running {
		v.internalUnlock()
		return
	}
	v.running = false
	close(v.reapChan)
	v.internalUnlock()
	v.processPool.WaitAll()
	v.internalLock()
	v.processPool.Release()
	defer v.internalUnlock()
	for _, conn := range v.live {
		conn.Close()
	}
	fmt.Println("Stopped vault at", v.rootPath)
}

func (v *Vault) assertLock() {
	v.mtx.Lock()
	v.mtx.Unlock()
}

func (v *Vault) internalLock() {
	v.mtx.Lock()
}

func (v *Vault) internalUnlock() {
	v.mtx.Unlock()
}

func (v *Vault) addConnection(va *VaultAddress) *VaultConnection {
	va.createFolder()
	vc := NewVaultConnection(va.FolderPath, va.FilePath)
	v.live[va.Id] = vc
	return vc
}

func (v *Vault) closeConnection(id uint32) {
	v.live[id].Close()
	delete(v.live, id)
}

func (v *Vault) getConnection(va *VaultAddress) *VaultConnection {
	for {
		v.internalLock()
		if len(v.live) < v.maxLive {
			vc := v.addConnection(va)
			v.internalUnlock()
			return vc
		} else {
			var lastId uint32
			var lastSeen time.Time = time.Now()
			var foundConn bool = false
			for id, conn := range v.live {
				if !conn.Busy() {
					if lastSeen.After(conn.lastUsed) {
						lastSeen = conn.lastUsed
						lastId = id
						foundConn = true
					}
				}
			}
			if foundConn {
				v.closeConnection(lastId)
				vc := v.addConnection(va)
				v.internalUnlock()
				return vc
			}
		}
		v.internalUnlock()
		runtime.Gosched()
	}
}

func (v *Vault) Write(did address.Did, key []byte, data *bytes.Buffer) {
	va := v.NewAddress(did.Hash())
	wo := NewVaultWrite(v, va, key, data)
	v.processPool.WaitCount(1)
	v.processPool.JobQueue <- wo.PoolExecute
}

func (v *Vault) Delete(did address.Did) {
	va := v.NewAddress(did.Hash())
	do := NewVaultDelete(v, va)
	v.processPool.WaitCount(1)
	v.processPool.JobQueue <- do.PoolExecute
}

func (v *Vault) Read(did address.Did, key []byte) *bytes.Buffer {
	va := v.NewAddress(did.Hash())
	ro := NewVaultRead(v, va, key)
	v.processPool.WaitCount(1)
	v.processPool.JobQueue <- ro.PoolExecute
	select {
	case buf, closed := <-ro.Return:
		if closed {
			return nil
		} else {
			return buf
		}
	}
}

func (v *Vault) reap() {
	v.internalLock()
	defer v.internalUnlock()
	reapable := make([]uint32, 0, len(v.live))
	for id, conn := range v.live {
		if !conn.Busy() {
			if time.Since(conn.lastUsed) > v.reapTimeout {
				reapable = append(reapable, id)
			}
		}
	}
	for _, id := range reapable {
		delete(v.live, id)
	}
}

func (v *Vault) reaper() {
	for {
		select {
		case _, closed := <-v.reapChan:
			if closed {
				return
			}
		case <-time.After(v.reapTimeout):
			v.reap()
		}
	}
}
