package vault

import (
	"github.com/boltdb/bolt"
	"sync"
	"time"
)

type VaultConnection struct {
	folderPath string
	filePath   string
	bolt       *bolt.DB
	mtx        sync.Mutex
	lastUsed   time.Time
	inUse      bool
}

func NewVaultConnection(folderPath, filePath string) *VaultConnection {
	return &VaultConnection{folderPath: folderPath, filePath: filePath, lastUsed: time.Now()}
}

func (vc *VaultConnection) open() {
	if vc.bolt != nil {
		return
	}
	if boltConn, err := bolt.Open(vc.filePath, 0644, nil); err != nil {
		panic(err)
	} else {
		vc.bolt = boltConn
	}
}

func (vc *VaultConnection) internalLock() {
	vc.mtx.Lock()
	vc.inUse = true
}

func (vc *VaultConnection) internalUnlock() {
	vc.inUse = false
	vc.lastUsed = time.Now()
	vc.mtx.Unlock()
}

func (vc *VaultConnection) Update(f func(tx *bolt.Tx) error) error {
	vc.internalLock()
	defer vc.internalUnlock()
	vc.open()
	return vc.bolt.Update(f)
}

func (vc *VaultConnection) View(f func(tx *bolt.Tx) error) error {
	vc.internalLock()
	defer vc.internalUnlock()
	vc.open()
	return vc.bolt.View(f)
}

func (vc *VaultConnection) Close() {
	vc.internalLock()
	defer vc.internalUnlock()
	if vc.bolt != nil {
		vc.bolt.Close()
	}
}

func (vc *VaultConnection) Busy() bool {
	return vc.inUse
}
