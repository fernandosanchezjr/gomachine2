package gomachine

import (
	"fmt"
	"net/http"
)

type getHandlerFunc func(*httpServer, *httpGetDataParams, *ResponseWriter)
type putHandlerFunc func(*httpServer, *httpPutDataParams, *ResponseWriter)

func binDataPostDeleteHandler(ms *httpServer, nw *ResponseWriter, r *http.Request, getHandler getHandlerFunc,
	deleteHandler getHandlerFunc) {
	if hgp, err := newHttpGetDataParams(r.Body); err != nil {
		nw.WriteHeader(http.StatusInternalServerError)
		nw.Write([]byte(fmt.Sprintf("Error parsing request body: %s", err)))
		return
	} else {
		switch r.Method {
		case "POST":
			getHandler(ms, hgp, nw)
		case "DELETE":
			deleteHandler(ms, hgp, nw)
		}
	}
}

func binDataPutHandler(ms *httpServer, nw *ResponseWriter, r *http.Request,
	putHandler putHandlerFunc) {
	if hpp, err := newHttpPutDataParams(r.Body); err != nil {
		nw.WriteHeader(http.StatusInternalServerError)
		nw.Write([]byte(fmt.Sprintf("Error parsing request body: %s", err)))
		return
	} else {
		putHandler(ms, hpp, nw)
	}
}

func binDataHandler(ms *httpServer, w http.ResponseWriter, r *http.Request, getHandler getHandlerFunc,
	putHandler putHandlerFunc, deleteHandler getHandlerFunc) {
	if !ms.checkStopped(w, r) {
		return
	}
	if !ms.checkMethods([]string{"POST", "PUT", "DELETE"}, w, r) {
		return
	}
	if !ms.checkContentType("application/json", w, r) {
		return
	}
	if !ms.parseForm(w, r) {
		return
	}
	nw := NewResponseWriter(w)
	switch r.Method {
	case "POST", "DELETE":
		binDataPostDeleteHandler(ms, nw, r, getHandler, deleteHandler)
	case "PUT":
		binDataPutHandler(ms, nw, r, putHandler)
	}
}

func getDataHandler(ms *httpServer, hgp *httpGetDataParams, w *ResponseWriter) {
	if hgp.Key == "" {
		w.WriteJSON(ms.m.DirectGetKeys(hgp.Did))
	} else {
		result, found := ms.m.DirectGet(hgp.Did, hgp.Key)
		w.WriteJSON(newHttpGetDataResponse(result, found))
	}
}

func putDataHandler(ms *httpServer, hpp *httpPutDataParams, w *ResponseWriter) {
	ms.m.DirectPut(hpp.Did, hpp.Key, hpp.Value)
	w.WriteJSON(true)
}

func deleteDataHandler(ms *httpServer, hgp *httpGetDataParams, w *ResponseWriter) {
	if hgp.Key == "" {
		ms.m.DirectClearData(hgp.Did)
	} else {
		ms.m.DirectDelete(hgp.Did, hgp.Key)
	}
	w.WriteJSON(true)
}

func dataHandler(ms *httpServer, w http.ResponseWriter, r *http.Request) {
	binDataHandler(ms, w, r, getDataHandler, putDataHandler, deleteDataHandler)
}

func getMetaHandler(ms *httpServer, hgp *httpGetDataParams, w *ResponseWriter) {
	if hgp.Key == "" {
		w.WriteJSON(ms.m.DirectGetMetaKeys(hgp.Did))
	} else {
		result, found := ms.m.DirectGetMeta(hgp.Did, hgp.Key)
		w.WriteJSON(newHttpGetDataResponse(result, found))
	}
}

func putMetaHandler(ms *httpServer, hpp *httpPutDataParams, w *ResponseWriter) {
	ms.m.DirectPutMeta(hpp.Did, hpp.Key, hpp.Value)
	w.WriteJSON(true)
}

func deleteMetaHandler(ms *httpServer, hgp *httpGetDataParams, w *ResponseWriter) {
	if hgp.Key == "" {
		ms.m.DirectClearMeta(hgp.Did)
		w.WriteJSON(true)
	} else {
		ms.m.DirectDeleteMeta(hgp.Did, hgp.Key)
		w.WriteJSON(true)
	}
}

func metaHandler(ms *httpServer, w http.ResponseWriter, r *http.Request) {
	binDataHandler(ms, w, r, getMetaHandler, putMetaHandler, deleteMetaHandler)
}
