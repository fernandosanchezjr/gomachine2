package newcache

import (
	"fmt"
	"testing"
	"time"
)

func TestBasicCache(t *testing.T) {
	cache := NewCache(time.Duration(100)*time.Millisecond, 1024)
	for i := 0; i < 1000; i++ {
		cache.Put(fmt.Sprint(i), i)
	}
	for i := 0; i < 1000; i++ {
		if _, found := cache.Get(fmt.Sprint(i)); !found {
			t.Fatal("Previously set entry", i, "not found")
		}
	}
	cache.Wait()
}

func BenchmarkCacheWrite(b *testing.B) {
	cache := NewCache(time.Duration(100)*time.Millisecond, 1024)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		cache.Put(fmt.Sprint(i), i)
	}
	b.StopTimer()
	cache.Wait()
}

func BenchmarkCacheRead(b *testing.B) {
	cache := NewCache(time.Duration(100)*time.Millisecond, 1024)
	for i := 0; i < b.N; i++ {
		cache.Get(fmt.Sprint(i))
	}
	b.StopTimer()
	cache.Wait()
}
