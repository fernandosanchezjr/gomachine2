package newcache

import (
	"fmt"
	"gomachine/operations"
	"time"
)

type Entry struct {
	id         string
	value      interface{}
	cache      *Cache
	ticker     *time.Ticker
	opChan     chan EntryOperation
	wrapUpChan chan int
	count      *operations.Count
	lastUsed   time.Time
	waiter     *operations.Wait
	wrappingUp bool
}

func NewEntry(id string, value interface{}, c *Cache) *Entry {
	ce := &Entry{id: id, value: value, cache: c, ticker: time.NewTicker(c.timeout),
		opChan: make(chan EntryOperation, 256), waiter: operations.NewWait(),
		wrapUpChan: make(chan int, 16)}
	ce.count = operations.NewCount(ce.onOperationsComplete)
	go ce.loop()
	return ce
}

func (ce *Entry) String() string {
	return fmt.Sprint("<Entry ", ce.id, ">")
}

func (ce *Entry) Id() string {
	return ce.id
}

func (ce *Entry) Value() interface{} {
	return ce.value
}

func (ce *Entry) SetValue(value interface{}) {
	ce.value = value
}

func (ce *Entry) StartOperation() {
	ce.cache.StartOperation()
	ce.count.StartOperation()
	ce.lastUsed = time.Now()
}

func (ce *Entry) OperationComplete() {
	ce.count.OperationComplete()
	ce.cache.OperationComplete()
}

func (ce *Entry) Busy() bool {
	if len(ce.opChan) > 0 {
		return true
	}
	if b, ok := ce.value.(operations.Busy); ok {
		if b.Busy() {
			return true
		}
	}
	return ce.count.Busy()
}

func (ce *Entry) Age() time.Duration {
	return time.Since(ce.lastUsed)
}

func (ce *Entry) recoverError() {
	recover()
}

func (ce *Entry) QueueOperation(op EntryOperation) (result bool) {
	defer ce.recoverError()
	ce.StartOperation()
	ce.opChan <- op
	result = true
	return
}

func (ce *Entry) executeOperation(op EntryOperation) {
	op.ExecuteEntryOperation(ce)
	ce.lastUsed = time.Now()
	ce.OperationComplete()
}

func (ce *Entry) attemptExpire() bool {
	if !ce.Busy() {
		ce.cache.entrymtx.Lock()
		defer ce.cache.entrymtx.Unlock()
		if ce.Age() >= ce.cache.timeout {
			close(ce.opChan)
			ce.ticker.Stop()
			ce.onOperationsComplete()
			ce.cache.deleteEntry(ce.id)
			return true
		}
	}
	return false
}

func (ce *Entry) onOperationsComplete() {
	ce.waiter.Notify()
}

func (ce *Entry) Wait() {
	if !ce.Busy() {
		return
	}
	ce.waiter.Wait()
}

func (ce *Entry) WrapUp() {
	ce.wrapUpChan <- 1
}

func (ce *Entry) loop() {
	for {
		if ce.wrappingUp {
			if ce.attemptExpire() {
				return
			}
		}
		select {
		case op := <-ce.opChan:
			ce.executeOperation(op)
		case <-ce.wrapUpChan:
			ce.wrappingUp = true
		case <-ce.ticker.C:
			if ce.attemptExpire() {
				return
			}
		}
	}
}
