package newcache

type PutOperation struct {
	id    string
	value interface{}
}

func NewPutOperation(id string, value interface{}) *PutOperation {
	return &PutOperation{id: id, value: value}
}

func (op *PutOperation) ExecuteCacheOperation(c *Cache) {
	c.entrymtx.Lock()
	defer c.entrymtx.Unlock()
	if entry, found := c.getEntry(op.id); !found {
		c.putEntry(op.id, c.NewEntry(op.id, op.value))
	} else {
		entry.QueueOperation(op)
	}
}

func (op *PutOperation) ExecuteEntryOperation(c *Entry) {
	c.SetValue(c.value)
}
