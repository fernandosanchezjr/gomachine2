package newcache

type CacheOperation interface {
	ExecuteCacheOperation(*Cache)
}

type EntryOperation interface {
	ExecuteEntryOperation(*Entry)
}
