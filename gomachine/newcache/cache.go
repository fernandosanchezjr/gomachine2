package newcache

import (
	"gomachine/operations"
	"sync"
	"time"
)

type Cache struct {
	timeout  time.Duration
	opChan   chan CacheOperation
	count    *operations.Count
	entries  map[string]*Entry
	entrymtx sync.Mutex
	waiter   *operations.Wait
}

func NewCache(timeout time.Duration, buffsize int) *Cache {
	c := &Cache{timeout: timeout, opChan: make(chan CacheOperation, buffsize), entries: make(map[string]*Entry),
		waiter: operations.NewWait()}
	c.count = operations.NewCount(c.onOperationsComplete)
	go c.loop()
	return c
}

func (c *Cache) Get(id string) (interface{}, bool) {
	op := NewGetOperation(id)
	c.QueueOperation(op)
	return op.Wait()
}

func (c *Cache) Put(id string, value interface{}) {
	c.QueueOperation(NewPutOperation(id, value))
}

func (c *Cache) NewEntry(id string, value interface{}) *Entry {
	return NewEntry(id, value, c)
}

func (c *Cache) getEntry(id string) (*Entry, bool) {
	entry, found := c.entries[id]
	return entry, found
}

func (c *Cache) GetEntry(id string) (*Entry, bool) {
	c.entrymtx.Lock()
	defer c.entrymtx.Unlock()
	return c.getEntry(id)
}

func (c *Cache) putEntry(id string, ce *Entry) *Entry {
	c.StartOperation()
	c.entries[id] = ce
	return ce
}

func (c *Cache) PutEntry(id string, ce *Entry) *Entry {
	c.entrymtx.Lock()
	defer c.entrymtx.Unlock()
	return c.putEntry(id, ce)
}

func (c *Cache) deleteEntry(id string) {
	delete(c.entries, id)
	c.OperationComplete()
}

func (c *Cache) DeleteEntry(id string) {
	c.entrymtx.Lock()
	defer c.entrymtx.Unlock()
	c.deleteEntry(id)
}

func (c *Cache) StartOperation() {
	c.count.StartOperation()
}

func (c *Cache) OperationComplete() {
	c.count.OperationComplete()
}

func (c *Cache) Busy() bool {
	if len(c.opChan) > 0 {
		return true
	}
	return c.count.Busy()
}

func (c *Cache) QueueOperation(op CacheOperation) {
	c.StartOperation()
	c.opChan <- op
}

func (c *Cache) executeOperation(op CacheOperation) {
	op.ExecuteCacheOperation(c)
	c.OperationComplete()
}

func (c *Cache) Wait() {
	if !c.Busy() {
		return
	}
	c.waiter.Wait()
}

func (c *Cache) onOperationsComplete() {
	c.waiter.Notify()
}

func (c *Cache) size() int {
	return len(c.entries)
}

func (c *Cache) Size() int {
	c.entrymtx.Lock()
	defer c.entrymtx.Unlock()
	return c.size()
}

func (c *Cache) loop() {
	for {
		select {
		case op := <-c.opChan:
			c.executeOperation(op)
		}
	}
}
