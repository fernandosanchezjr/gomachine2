package newcache

type GetOperationResult struct {
	Value interface{}
	Found bool
}

type GetOperation struct {
	id      string
	retChan chan GetOperationResult
}

func NewGetOperation(id string) *GetOperation {
	return &GetOperation{id: id, retChan: make(chan GetOperationResult)}
}

func (op *GetOperation) ExecuteCacheOperation(c *Cache) {
	c.entrymtx.Lock()
	defer c.entrymtx.Unlock()
	if entry, found := c.getEntry(op.id); !found {
		op.retChan <- GetOperationResult{Value: nil, Found: false}
	} else {
		entry.QueueOperation(op)
	}
}

func (op *GetOperation) ExecuteEntryOperation(c *Entry) {
	op.retChan <- GetOperationResult{Value: c.Value(), Found: true}
}

func (op *GetOperation) Wait() (interface{}, bool) {
	select {
	case ret := <-op.retChan:
		return ret.Value, ret.Found
	}
}
