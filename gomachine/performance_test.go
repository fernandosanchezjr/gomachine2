package gomachine

import (
	"fmt"
	"gomachine/address"
	"testing"
	"time"
)

func TestNumberStrategy(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping TestNumberStrategy in short mode")
	}
	m := createTestMachine("TestNumberStrategy")
	m.Wait()
	start := time.Now()
	if err := m.DirectPutBeat(address.NewDid("test"), "PutId", `func(bin, id) {
       bin.Put("Id", id)
       println("Storing", id, "in", bin.Did())
	}`); err != nil {
		t.Fatal(err)
	}
	if _, err := m.Execute(`
	func TestBeatPopulation(max) {
	  println("Starting TestBeatPopulation")
	  for i = 0; i < max; i++ {
	    did = gomachine.DidPrepend(strategies.Number(i), "test")
	    machine.GoDirectBeat(did, "PutId", i)
	  }
	  println("Done with TestBeatPopulation")
	}

	TestBeatPopulation(100000)
	`); err != nil {
		t.Fatal(err)
	}
	fmt.Println("Processed in", time.Since(start))
	fmt.Println("Waiting...")
	m.Wait()
	fmt.Println("Total elapsed", time.Since(start))
	fmt.Println("done.")
}
