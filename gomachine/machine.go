package gomachine

import (
	"fmt"
	"github.com/fernandosanchezjr/anko/ast"
	"github.com/fernandosanchezjr/anko/vm"
	"github.com/nightlyone/lockfile"
	"gomachine/address"
	"gomachine/ioutil"
	"gomachine/newcache"
	"gomachine/newvault"
	"gomachine/operations"
	"net/http"
	"os"
	"os/signal"
	"path"
	"runtime/debug"
	"sync"
	"time"
)

type LifetimeCallback func(*Machine) error

type Machine struct {
	Bin
	creatorMtx   sync.Mutex
	count        *operations.Count
	waiter       *operations.Wait
	name         string
	path         string
	vm           *ankoVM
	cache        *newcache.Cache
	lockFile     lockfile.Lockfile
	onShutdown   []LifetimeCallback
	exitchannel  chan os.Signal
	httpServer   *httpServer
	cleanupCount int
	vault        *newvault.Vault
	cacheTtl     time.Duration
}

type directResult struct {
	result interface{}
	err    error
}

type getResult struct {
	value interface{}
	found bool
}

type getBeatResult struct {
	value *VMCode
	found bool
}

func SetMachineRoot(root string) {
	machineRoot = root
}

func NewMachine(name string, startup ...LifetimeCallback) *Machine {
	ParseFlags()
	machine := &Machine{name: name, cache: newcache.NewCache(cacheTTL, 1024),
		exitchannel: make(chan os.Signal, 1), onShutdown: make([]LifetimeCallback, 0), waiter: operations.NewWait(),
		cacheTtl: cacheTTL}
	machine.count = operations.NewCount(machine.onOperationsComplete)
	machine.init(startup...)
	return machine
}

func (m *Machine) init(startup ...LifetimeCallback) {
	m.generateStoragePath()
	m.CreatePath()
	m.lock()
	m.setupVault()
	m.Bin = newChildBin(address.Did([]interface{}{}), nil, m)
	signal.Notify(m.exitchannel, os.Interrupt, os.Kill)
	m.vm = newAnkoVM()
	m.setVMBuiltins()
	m.httpServer = newMachineHTTPServer(m)
	m.httpServer.Serve()
	m.AddShutdownCallback(m.stopHttpServer)
	m.nativeStartup(startup...)
	m.vmStartup()
}

func (m *Machine) setupVault() {
	m.vault = newvault.NewVault(path.Join(m.StoragePath(), "bins"), cacheTTL)
}

func (m *Machine) stopVault() {
	m.vault.Close()
}

func (m *Machine) stopHttpServer(*Machine) error {
	m.httpServer.stop()
	return nil
}

func (m *Machine) setVMBuiltins() {
	m.SetVMImport("gomachine", machineImport)
	m.SetVMImport("strategies", strategyImport)
	m.DefineVMValue("machine", m)
	m.DefineVMValue("exit", m.exit)
}

func (m *Machine) nativeStartup(startup ...LifetimeCallback) {
	for _, s := range startup {
		if err := s(m); err != nil {
			fmt.Println("Caught error during startup:", err)
		}
	}
}

func (m *Machine) nativeShutdown() {
	for _, s := range m.onShutdown {
		if err := s(m); err != nil {
			fmt.Println("Caught error during shutdown:", err)
		}
	}
}

func (m *Machine) vmStartup() {
	startupPath := path.Join(m.StoragePath(), "scripts", "startup.anko")
	if ioutil.PathExists(startupPath) {
		if _, err := m.vm.ExecuteFile(startupPath); err != nil {
			fmt.Println("Caught error during VM startup:", err)
		}
	}
}

func (m *Machine) vmShutdown() {
	startupPath := path.Join(m.StoragePath(), "scripts", "shutdown.anko")
	if ioutil.PathExists(startupPath) {
		if _, err := m.vm.ExecuteFile(startupPath); err != nil {
			fmt.Println("Caught error during VM shutdown:", err)
		}
	}
}

func (m *Machine) exit() {
	m.exitchannel <- os.Interrupt
}

func (m *Machine) lock() {
	lockPath := fmt.Sprintf("%s/%s.lock", m.path, m.name)
	var err error
	m.lockFile, err = lockfile.New(lockPath)
	if err != nil {
		fmt.Println(fmt.Sprintf("Error starting gomachine %s:", m.name), err)
		os.Exit(1)
	}
	err = m.lockFile.TryLock()
	if err != nil {
		fmt.Println(fmt.Sprintf("Error starting gomachine %s:", m.name), err)
		os.Exit(1)
	}
}

func (m *Machine) String() string {
	return m.Name()
}

func (m *Machine) Name() string {
	return m.name
}

func (m *Machine) StartOperation() {
	m.count.StartOperation()
}

func (m *Machine) OperationComplete() {
	m.count.OperationComplete()
}

func (m *Machine) onOperationsComplete() {
	m.waiter.Notify()
}

func (m *Machine) OperationsCompleteMonitor() {
	m.onOperationsComplete()
}

func (*Machine) IsPersistable() bool {
	return true
}

func (m *Machine) Busy() bool {
	if m.count.Busy() {
		return true
	}
	if m.cache.Busy() {
		return true
	}
	if m.vault.Busy() {
		return true
	}
	return false
}

func (m *Machine) Wait() {
	if m.count.Busy() {
		m.waiter.Wait()
	}
	m.cache.Wait()
	m.vault.Wait()
}

func (m *Machine) generateStoragePath() {
	m.path = path.Join(machineRoot, m.name)
}

func (m *Machine) CreatePath() {
	ioutil.CreatePath(m.path)
}

func (m *Machine) DeletePath() {
	ioutil.DeletePath(m.path)
}

func (m *Machine) ResetPath() {
	m.DeletePath()
	m.CreatePath()
}

func (m *Machine) StoragePath() string {
	return m.path
}

func (m *Machine) backwardsSearch(did address.Did) (Bin, address.Did) {
	didCopy := did
	rest := address.ToDid([]interface{}{})
	for len(didCopy) > 1 {
		rest = rest.Prepend(didCopy[len(didCopy)-1])
		didCopy = didCopy[:len(didCopy)-2]
		if fb, found := m.lookupBin(did); found {
			return fb, rest
		}
	}
	return nil, nil
}

func (m *Machine) DidWalk(did address.Did) Bin {
	if len(did) == 0 {
		return m.Bin
	}
	if fb, found := m.lookupBin(did); found {
		return fb
	}
	b := m.Bin
	if fb, rest := m.backwardsSearch(did); fb != nil {
		did = rest
		b = fb
	}
	for len(did) > 0 {
		did, b = m.didStep(b, did)
	}
	return b
}

func (m *Machine) didStep(b Bin, did address.Did) ([]interface{}, Bin) {
	id, rest := did[0], did[1:]
	child := b.AddChild(id)
	return rest, child
}

func (m *Machine) doDirect(ch chan *directResult, f func(b Bin) (interface{}, error), did []interface{}) {
	b := m.DidWalk(did)
	r, err := f(b)
	ch <- &directResult{result: r, err: err}
}

func (m *Machine) Direct(did address.Did, f func(b Bin) (interface{}, error)) (interface{}, error) {
	m.StartOperation()
	defer m.OperationComplete()
	ch := make(chan *directResult)
	go m.doDirect(ch, f, did)
	select {
	case r := <-ch:
		return r.result, r.err
	}
}

func (m *Machine) doGoDirect(f func(b Bin) (interface{}, error), did []interface{}) {
	defer m.OperationComplete()
	ch := make(chan *directResult)
	go m.doDirect(ch, f, did)
	select {
	case <-ch:
		return
	}
}

func (m *Machine) GoDirect(did address.Did, f func(b Bin) (interface{}, error)) {
	m.StartOperation()
	go m.doGoDirect(f, did)
}

func (m *Machine) DirectPut(did address.Did, key string, value interface{}) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.Put(key, value)
		return nil, nil
	})
}

func (m *Machine) DirectGetKeys(did address.Did) []string {
	r, _ := m.Direct(did, func(b Bin) (interface{}, error) {
		return b.GetKeys(), nil
	})
	kv, _ := r.([]string)
	return kv
}

func (m *Machine) DirectGet(did address.Did, key string) (interface{}, bool) {
	r, _ := m.Direct(did, func(b Bin) (interface{}, error) {
		value, found := b.Get(key)
		return &getResult{value: value, found: found}, nil
	})
	rv, _ := r.(*getResult)
	return rv.value, rv.found
}

func (m *Machine) DirectDelete(did address.Did, key string) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.Delete(key)
		return nil, nil
	})
}

func (m *Machine) DirectClearData(did address.Did) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.ClearData()
		return nil, nil
	})
}

func (m *Machine) DirectPutMeta(did address.Did, key string, value interface{}) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.PutMeta(key, value)
		return nil, nil
	})
}

func (m *Machine) DirectGetMetaKeys(did address.Did) []string {
	r, _ := m.Direct(did, func(b Bin) (interface{}, error) {
		return b.GetMetaKeys(), nil
	})
	kv, _ := r.([]string)
	return kv
}

func (m *Machine) DirectGetMeta(did address.Did, key string) (interface{}, bool) {
	r, _ := m.Direct(did, func(b Bin) (interface{}, error) {
		value, found := b.GetMeta(key)
		return &getResult{value: value, found: found}, nil
	})
	rv, _ := r.(*getResult)
	return rv.value, rv.found
}

func (m *Machine) DirectDeleteMeta(did address.Did, key string) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.DeleteMeta(key)
		return nil, nil
	})
}

func (m *Machine) DirectClearMeta(did address.Did) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.ClearMeta()
		return nil, nil
	})
}

func (m *Machine) registerBin(b Bin) {
	//m.cache.SetWithTTL(b.Did().String(), b, (cacheTTL + (time.Duration(ioutil.Random(1, 1000)) * time.Millisecond)))
	m.cache.Put(string(b.Did().Hash()), b)
}

func (m *Machine) retrieveBin(did address.Did) (Bin, bool) {
	if cached, found := m.cache.Get(string(did.Hash())); !found {
		return nil, false
	} else {
		if bin, ok := cached.(Bin); ok {
			return bin, true
		}
	}
	return nil, false
}

func (m *Machine) rawRetrieveBin(did address.Did) (Bin, bool) {
	m.cache.StartOperation()
	defer m.cache.OperationComplete()
	if cached, found := m.cache.GetEntry(string(did.Hash())); !found {
		return nil, false
	} else {
		cached.StartOperation()
		defer cached.OperationComplete()
		if bin, ok := cached.Value().(Bin); ok {
			return bin, true
		}
	}
	return nil, false
}

func (m *Machine) rawLookupBin(did address.Did) (Bin, bool) {
	m.creatorMtx.Lock()
	defer m.creatorMtx.Unlock()
	if did.Root() {
		return m.Bin, true
	}
	return m.rawRetrieveBin(did)
}

func (m *Machine) lookupBin(did address.Did) (Bin, bool) {
	m.creatorMtx.Lock()
	defer m.creatorMtx.Unlock()
	if did.Root() {
		return m.Bin, true
	}
	return m.retrieveBin(did)
}

func (m *Machine) lookupOrCreateBin(did address.Did, creator func(address.Did) Bin) (Bin, bool) {
	m.creatorMtx.Lock()
	defer m.creatorMtx.Unlock()
	if did.Root() {
		return m.Bin, true
	}
	if b, found := m.retrieveBin(did); found {
		return b, true
	} else {
		created := creator(did)
		m.registerBin(created)
		return created, false
	}
}

func (m *Machine) newBinCreator(did address.Did) Bin {
	parentDid := did[:did.Length()-1]
	return newChildBin(did, parentDid, m)
}

func (m *Machine) loadBin(did address.Did) Bin {
	bin, _ := m.lookupOrCreateBin(did, m.newBinCreator)
	return bin
}

func (m *Machine) removeBin(bin Bin) {
	m.creatorMtx.Lock()
	defer m.creatorMtx.Unlock()
	m.StartOperation()
	defer m.OperationComplete()
	parent := bin.Parent()
	parent.RemoveChild(bin.Id())
}

func (m *Machine) newCacheItemCallback(key string, value interface{}) {
	if _, ok := value.(Bin); ok {
		m.StartOperation()
	}
}

func (m *Machine) checkCacheRemovalCallback(key string, value interface{}) bool {
	m.cleanupCount += 1
	if m.cleanupCount == 1000 {
		m.cleanupCount = 0
		debug.FreeOSMemory()
	}
	if bin, ok := value.(*baseBin); ok {
		if bin.Busy() {
			return false
		}
	}
	return true
}

func (m *Machine) cacheRemovalCallback(key string, value interface{}) {
	if bin, ok := value.(*baseBin); ok {
		if bin.Empty() {
			go m.removeBin(bin)
		}
		m.OperationComplete()
	}
}

func (m *Machine) GetVMValue(name string) (interface{}, error) {
	return m.vm.Get(name)
}

func (m *Machine) DefineVMValue(name string, value interface{}) {
	m.vm.Define(name, value)
}

func (m *Machine) SetVMValue(name string, value interface{}) {
	m.vm.Set(name, value)
}

func (m *Machine) SetVMImport(name string, loader func(*vm.Env) *vm.Env) {
	m.vm.SetImport(name, loader)
}

func (m *Machine) Parse(code string) ([]ast.Stmt, error) {
	return m.vm.Parse(code)
}

func (m *Machine) Execute(source string) (interface{}, error) {
	m.StartOperation()
	defer m.OperationComplete()
	return m.vm.Execute(source)
}

func (m *Machine) Run(statements []ast.Stmt) (interface{}, error) {
	m.StartOperation()
	defer m.OperationComplete()
	return m.vm.Run(statements)
}

func (m *Machine) newChildVM() *ankoVM {
	return m.vm.NewChildEnv()
}

func (m *Machine) DirectPutBeat(did address.Did, name string, beat string) error {
	_, err := m.Direct(did, func(b Bin) (interface{}, error) {
		return nil, b.PutBeat(name, beat)
	})
	return err
}

func (m *Machine) DirectGetBeat(did address.Did, name string) (*VMCode, bool) {
	result, _ := m.Direct(did, func(b Bin) (interface{}, error) {
		value, found := b.GetBeat(name)
		return &getBeatResult{value: value, found: found}, nil
	})
	br, _ := result.(*getBeatResult)
	return br.value, br.found
}

func (m *Machine) DirectDeleteBeat(did address.Did, name string) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.DeleteBeat(name)
		return nil, nil
	})
}

func (m *Machine) DirectClearBeats(did address.Did) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.ClearBeats()
		return nil, nil
	})
}

func (m *Machine) DirectGetBeatNames(did address.Did) []string {
	r, _ := m.Direct(did, func(b Bin) (interface{}, error) {
		return b.GetBeatNames(), nil
	})
	kv, _ := r.([]string)
	return kv
}

func (m *Machine) DirectExecute(did address.Did, source string) (interface{}, error) {
	return m.Direct(did, func(b Bin) (interface{}, error) {
		return b.Execute(source)
	})
}

func (m *Machine) DirectBeat(did address.Did, name string, args ...interface{}) (interface{}, error) {
	return m.Direct(did, func(b Bin) (interface{}, error) {
		return b.Beat(name, args...)
	})
}

func (m *Machine) GoDirectBeat(did address.Did, name string, args ...interface{}) {
	m.GoDirect(did, func(b Bin) (interface{}, error) {
		return b.Beat(name, args...)
	})
}

func (m *Machine) DirectContextBeat(did address.Did, name string, context map[string]interface{},
	args ...interface{}) (interface{}, error) {
	return m.Direct(did, func(b Bin) (interface{}, error) {
		return b.ContextBeat(name, context, args...)
	})
}

func (m *Machine) MainLoop() {
	fmt.Println(m.String(), "started")
	select {
	case <-m.exitchannel:
		fmt.Println(fmt.Sprintf("Shutting down %s...", m.Name()))
		m.vmShutdown()
		m.nativeShutdown()
		m.Wait()
		m.stopVault()
		fmt.Println(fmt.Sprintf("%s down.", m.Name()))
	}
}

func (m *Machine) directBroadcast(bcast Broadcast) error {
	bcast.StartOperation()
	_, err := m.Direct(bcast.Did(), func(b Bin) (interface{}, error) {
		defer bcast.OperationComplete()
		b.Broadcast(bcast)
		return nil, nil
	})
	if err != nil {
		bcast.OperationComplete()
	}
	return err
}

func (m *Machine) newBroadcast(did address.Did, depth int, stopCheck func(Broadcast, Bin) bool,
	broadcastFunc func(b Bin) interface{}) Broadcast {
	return newBroadcast(did, m, depth, stopCheck, broadcastFunc, nil)
}

func (m *Machine) NewInquiry(did address.Did, depth int) Inquiry {
	return newVMInquiry(did, depth, m)
}

func (m *Machine) DirectClear(did address.Did) {
	m.Direct(did, func(b Bin) (interface{}, error) {
		b.Clear()
		return nil, nil
	})
}

func (m *Machine) BroadcastClear(did address.Did, depth int) {
	bcast := m.newBroadcast(did, depth, nil, func(b Bin) interface{} {
		b.Clear()
		return nil
	})
	m.directBroadcast(bcast)
}

func (m *Machine) AddShutdownCallback(s LifetimeCallback) {
	m.onShutdown = append(m.onShutdown, s)
}

func (m *Machine) Handle(pattern string, handler http.Handler) {
	m.httpServer.Handle(pattern, handler)
}

func (m *Machine) HandleFunc(pattern string, handler http.HandlerFunc) {
	m.httpServer.HandleFunc(pattern, handler)
}
