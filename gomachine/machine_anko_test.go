package gomachine

import (
	"fmt"
	"gomachine/address"
	"sync"
	"testing"
)

var jsExecCount int = 0
var beatExecCount int = 0
var broadcastExecCount int = 0
var inquiryExecCount int = 0

func TestBasicExecution(t *testing.T) {
	m := createTestMachine("TestBasicExecution")
	m.DirectPut(bin123Did, intKey, intValue)
	m.Wait()
	bin := m.DidWalk(bin123Did)
	if _, err := bin.Execute(`a = "test 1 2 3 "`); err != nil {
		t.Fatal("Error executing anko:", err)
	}
	m.Wait()
}

func TestBeatStorage(t *testing.T) {
	m := createTestMachine("TestBeatStorage")
	m.DirectPut(bin12Did, intKey, intValue)
	m.Wait()
	bin := m.DidWalk(bin12Did)
	if err := bin.PutBeat(testBeatName, testBeatBody); err != nil {
		t.Fatal("Error registering beat:", err)
	}
	beatObj, found := bin.GetBeat(testBeatName)
	if !found {
		t.Fatal("Beat not found")
	}
	if beatObj.String() != testBeatBody {
		t.Fatal("Retrieved beat does not match stored beat! received:", beatObj.String(), "- expected:", testBeatBody)
	}
	m.Wait()
}

func TestBeatInheritance(t *testing.T) {
	m := createTestMachine("TestBeatInheritance")
	m.DirectPut(bin12Did, intKey, intValue)
	m.Wait()
	bin := m.DidWalk(bin12Did)
	if err := bin.PutBeat(testBeatName, testBeatBody); err != nil {
		t.Fatal("Error registering beat:", err)
	}
	bin = m.DidWalk(bin123Did)
	beatObj, found := bin.GetBeat(testBeatName)
	if !found {
		t.Fatal("Beat not found")
	}
	if beatObj.String() != testBeatBody {
		t.Fatal("Retrieved beat does not match stored beat! received:", beatObj.String(), "- expected:", testBeatBody)
	}
	m.Wait()
}

func TestBeatExecution(t *testing.T) {
	m := createTestMachine("TestBeatExecution")
	m.DirectPut(bin12Did, intKey, intValue)
	m.Wait()
	bin := m.DidWalk(bin12Did)
	if err := bin.PutBeat(testBeatName, testBeatBody); err != nil {
		t.Fatal("Error registering beat:", err)
	}
	beatObj, found := bin.GetBeat(testBeatName)
	if !found {
		t.Fatal("Beat not found")
	}
	if beatObj.String() != testBeatBody {
		t.Fatal("Retrieved beat does not match stored beat! received:", beatObj.String(), "- expected:", testBeatBody)
	}
	_, err := bin.Beat(testBeatName, 1, 2)
	if err != nil {
		t.Fatal("Error executing beat:", err)
	}
	m.Wait()
}

func TestDirectBeatExecution(t *testing.T) {
	m := createTestMachine("TestDirectBeatExecution")
	m.DirectPut(bin12Did, intKey, intValue)
	m.DirectPutBeat(bin12Did, testBeatName, testBeatBody)
	m.Wait()
	_, err := m.DirectBeat(bin12Did, testBeatName, 1, 2)
	if err != nil {
		t.Fatal("Error executing beat:", err)
	}
	m.Wait()
}

func TestDirectBroadcast(t *testing.T) {
	m := createTestMachine("TestDirectBroadcast")
	var reachedMtx sync.Mutex
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		reachedDids[did.String()] = did
	}
	m.Wait()
	bcast := m.newBroadcast([]interface{}{1}, UnlimitedBroadcast, nil, func(b Bin) interface{} {
		reachedMtx.Lock()
		defer reachedMtx.Unlock()
		delete(reachedDids, b.Did().String())
		return nil
	})
	if err := bcast.Start(); err != nil {
		t.Fatal("Error starting broadcast", err)
	}
	bcast.Wait()
	if len(reachedDids) != 0 {
		t.Fatalf("Broadcast failed to reach some dids: %#v", reachedDids)
	}
	m.Wait()
}

func TestDirectBroadcastStop(t *testing.T) {
	m := createTestMachine("TestDirectBroadcastStop")
	var reachedMtx sync.Mutex
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		reachedDids[did.String()] = did
	}
	m.Wait()
	bcast := m.newBroadcast([]interface{}{1}, UnlimitedBroadcast, func(bcast Broadcast, b Bin) bool {
		return b.Did().Equals(bin12Did)
	}, func(b Bin) interface{} {
		reachedMtx.Lock()
		defer reachedMtx.Unlock()
		delete(reachedDids, b.Did().String())
		return nil
	})
	if err := bcast.Start(); err != nil {
		t.Fatal("Error starting broadcast", err)
	}
	bcast.Wait()
	for didStr, did := range reachedDids {
		if !did.StartsWith(bin12Did) {
			t.Fatal("Broadcast failed to reach", didStr, "outside of the stop tree")
		}
	}
	m.Wait()
}

func TestDirectBroadcastStopNumber(t *testing.T) {
	m := createTestMachine("TestDirectBroadcastStopNumber")
	var reachedMtx sync.Mutex
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		reachedDids[did.String()] = did
	}
	m.Wait()
	bcast := m.newBroadcast([]interface{}{1}, 2, nil, func(b Bin) interface{} {
		reachedMtx.Lock()
		defer reachedMtx.Unlock()
		delete(reachedDids, b.Did().String())
		return nil
	})
	if err := bcast.Start(); err != nil {
		t.Fatal("Error starting broadcast", err)
	}
	bcast.Wait()
	for didStr, did := range reachedDids {
		if !layer3Dids.Contains(did) && layer2Dids.Contains(did) && layer1Dids.Contains(did) {
			t.Fatal("Broadcast failed to reach", didStr, "outside of the stop tree")
		}
	}
	m.Wait()
}

func TestDirectInquiry(t *testing.T) {
	m := createTestMachine("TestDirectInquiry")
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		if !did.StartsWith(bin12Did) {
			reachedDids[did.String()] = did
		}
	}
	m.Wait()
	inquiry := m.NewInquiry([]interface{}{1}, UnlimitedBroadcast)
	inquiry.Put("reachedDids", reachedDids)
	if err := inquiry.SetExecute(`
		inquiry.AccumulateToMaster(nil)
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.SetAccumulate(`
		rdids, _ = inquiry.Get("reachedDids")
		delete(rdids, gomachine.DidString(from))
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.Start(); err != nil {
		t.Fatal("Error starting inquiry", err)
	}
	inquiry.Wait()
	if inquiry.Error() != nil {
		t.Fatal("Error running inquiry", inquiry.Error())
	}
	if len(reachedDids) > 0 {
		t.Fatal("Broadcast inquiry failed to reach", reachedDids)
	}
	m.Wait()
}

func TestDirectInquiryStopFunc(t *testing.T) {
	m := createTestMachine("TestDirectInquiryStopFunc")
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		if !did.StartsWith(bin12Did) {
			reachedDids[did.String()] = did
		}
	}
	m.Wait()
	inquiry := m.NewInquiry([]interface{}{1}, UnlimitedBroadcast)
	inquiry.Put("reachedDids", reachedDids)
	if err := inquiry.SetStop(`
		gomachine.DidEquals([1,2], bin.Did())
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.SetExecute(`
		inquiry.AccumulateToMaster(nil)
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.SetAccumulate(`
		rdids, _ = inquiry.Get("reachedDids")
		delete(rdids, gomachine.DidString(from))
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.Start(); err != nil {
		t.Fatal("Error starting inquiry", err)
	}
	inquiry.Wait()
	if inquiry.Error() != nil {
		t.Fatal("Error running inquiry", inquiry.Error())
	}
	if len(reachedDids) > 0 {
		t.Fatal("Broadcast inquiry failed to reach", reachedDids)
	}
	m.Wait()
}

func TestDirectInquiryCompleteFunc(t *testing.T) {
	m := createTestMachine("TestDirectInquiryCompleteFunc")
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		if did.Equals(bin1Did) {
			reachedDids[did.String()] = did
		}
	}
	m.Wait()
	inquiry := m.NewInquiry([]interface{}{1}, UnlimitedBroadcast)
	inquiry.Put("reachedDids", reachedDids)
	if err := inquiry.SetStop(`
		gomachine.DidEquals([1,2], bin.Did())
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.SetComplete(`
		inquiry.AccumulateToMaster(nil)
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.SetAccumulate(`
		rdids, _ = inquiry.Get("reachedDids")
		delete(rdids, gomachine.DidString(from))
	`); err != nil {
		t.Fatal("Invalid StopFunc source:", err)
	}
	if err := inquiry.Start(); err != nil {
		t.Fatal("Error starting inquiry", err)
	}
	inquiry.Wait()
	if inquiry.Error() != nil {
		t.Fatal("Error running inquiry", inquiry.Error())
	}
	if len(reachedDids) > 0 {
		t.Fatal("Broadcast inquiry failed to reach", reachedDids)
	}
	m.Wait()
}

func BenchmarkExecution(b *testing.B) {
	jsExecCount = jsExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkExecution-%d", jsExecCount))
	m.DirectPut(bin123Did, intKey, intValue)
	m.Wait()
	bin := m.DidWalk(bin123Did)
	bin.StartOperation()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if _, err := bin.Execute(fmt.Sprintf("bin.Put(\"result\", (%d + %d));", i, i+1)); err != nil {
			b.Fatal("Error executing source:", err)
		}
	}
	b.StopTimer()
	bin.OperationComplete()
	m.Wait()
}

func BenchmarkDirectExecution(b *testing.B) {
	jsExecCount = jsExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkDirectExecution-%d", jsExecCount))
	m.DirectPut(bin123Did, intKey, intValue)
	m.Wait()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if _, err := m.DirectExecute(bin123Did, fmt.Sprintf("bin.Put(\"result\", (%d + %d));", i, i+1)); err != nil {
			b.Fatal("Error executing source:", err)
		}
	}
	b.StopTimer()
	m.Wait()
}

func BenchmarkBeatExecution(b *testing.B) {
	beatExecCount = beatExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkBeatExecution-%d", beatExecCount))
	m.DirectPutBeat(bin12Did, testBeatName, testBeatBody)
	m.DirectPut(bin123Did, intKey, intValue)
	m.Wait()
	bin := m.DidWalk(bin123Did)
	bin.StartOperation()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if _, err := bin.Beat(testBeatName, i, i+1); err != nil {
			b.Fatal("Error executing beat:", err)
		}
	}
	b.StopTimer()
	bin.OperationComplete()
	m.Wait()
}

func BenchmarkDirectBeatExecution(b *testing.B) {
	beatExecCount = beatExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkDirectBeatExecution-%d", beatExecCount))
	m.DirectPutBeat(bin12Did, testBeatName, testBeatBody)
	m.DirectPut(bin123Did, intKey, intValue)
	m.Wait()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if _, err := m.DirectBeat(bin123Did, testBeatName, i, i+1); err != nil {
			b.Fatal("Error executing beat:", err)
		}
	}
	b.StopTimer()
	m.Wait()
}

func BenchmarkBroadcast(b *testing.B) {
	broadcastExecCount = broadcastExecCount + 1
	broadcastName := fmt.Sprintf("BenchmarkBroadcast-%d", broadcastExecCount)
	m := createTestMachine(broadcastName)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
	}
	m.Wait()
	b.StopTimer()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var reachedmtx sync.Mutex
		reachedDids := make(map[string]address.Did)
		for _, did := range allLayerDids {
			reachedDids[did.String()] = did
		}
		bcast := m.newBroadcast([]interface{}{1}, UnlimitedBroadcast, nil, func(b Bin) interface{} {
			reachedmtx.Lock()
			defer reachedmtx.Unlock()
			delete(reachedDids, b.Did().String())
			return nil
		})
		b.StartTimer()
		if err := bcast.Start(); err != nil {
			b.Fatal("Error starting broadcast", err)
		}
		bcast.Wait()
		b.StopTimer()
		if len(reachedDids) != 0 {
			b.Fatalf("Broadcast %s %d failed to reach some dids: %#v", broadcastName, i, reachedDids)
		}
	}
	m.Wait()
}

func BenchmarkDirectInquiry(b *testing.B) {
	inquiryExecCount = inquiryExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkDirectInquiry-%d", broadcastExecCount))
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		if !did.StartsWith(bin12Did) {
			reachedDids[did.String()] = did
		}
	}
	m.Wait()
	b.StopTimer()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tempReachedDids := make(map[string]address.Did)
		for key, val := range reachedDids {
			tempReachedDids[key] = val
		}
		inquiry := m.NewInquiry([]interface{}{1}, UnlimitedBroadcast)
		inquiry.Put("reachedDids", tempReachedDids)
		if err := inquiry.SetExecute(`
			inquiry.AccumulateToMaster(nil)
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		if err := inquiry.SetAccumulate(`
			rdids, _ = inquiry.Get("reachedDids")
			delete(rdids, gomachine.DidString(from))
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		b.StartTimer()
		if err := inquiry.Start(); err != nil {
			b.Fatal("Error starting inquiry", err)
		}
		inquiry.Wait()
		b.StopTimer()
		if inquiry.Error() != nil {
			b.Fatal("Error running inquiry", inquiry.Error())
		}
		if len(tempReachedDids) > 0 {
			b.Fatal("Broadcast inquiry failed to reach", reachedDids)
		}
	}
	m.Wait()
}

func BenchmarkDirectInquiryStopFunc(b *testing.B) {
	inquiryExecCount = inquiryExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkDirectInquiryStopFunc-%d", inquiryExecCount))
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		if !did.StartsWith(bin12Did) {
			reachedDids[did.String()] = did
		}
	}
	m.Wait()
	b.StopTimer()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tempReachedDids := make(map[string]address.Did)
		for key, val := range reachedDids {
			tempReachedDids[key] = val
		}
		inquiry := m.NewInquiry([]interface{}{1}, UnlimitedBroadcast)
		if err := inquiry.SetStop(`
			gomachine.DidEquals([1,2], bin.Did())
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		inquiry.Put("reachedDids", tempReachedDids)
		if err := inquiry.SetExecute(`
			inquiry.AccumulateToMaster(nil)
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		if err := inquiry.SetAccumulate(`
			rdids, _ = inquiry.Get("reachedDids")
			delete(rdids, gomachine.DidString(from))
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		b.StartTimer()
		if err := inquiry.Start(); err != nil {
			b.Fatal("Error starting inquiry", err)
		}
		inquiry.Wait()
		b.StopTimer()
		if inquiry.Error() != nil {
			b.Fatal("Error running inquiry", inquiry.Error())
		}
		if len(tempReachedDids) > 0 {
			b.Fatal("Broadcast inquiry failed to reach", reachedDids)
		}
	}
	m.Wait()
}

func BenchmarkDirectInquiryCompleteFunc(b *testing.B) {
	inquiryExecCount = inquiryExecCount + 1
	m := createTestMachine(fmt.Sprintf("BenchmarkDirectInquiryCompleteFunc-%d", inquiryExecCount))
	reachedDids := make(map[string]address.Did)
	for _, did := range allLayerDids {
		m.DirectPut(did, intKey, intValue)
		m.DirectPut(did, strKey, strValue)
		if did.Equals(bin1Did) {
			reachedDids[did.String()] = did
		}
	}
	m.Wait()
	b.StopTimer()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tempReachedDids := make(map[string]address.Did)
		for key, val := range reachedDids {
			tempReachedDids[key] = val
		}
		inquiry := m.NewInquiry([]interface{}{1}, UnlimitedBroadcast)
		if err := inquiry.SetStop(`
			gomachine.DidEquals([1,2], bin.Did())
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		inquiry.Put("reachedDids", tempReachedDids)
		if err := inquiry.SetComplete(`
			inquiry.AccumulateToMaster(nil)
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		if err := inquiry.SetAccumulate(`
			rdids, _ = inquiry.Get("reachedDids")
			delete(rdids, gomachine.DidString(from))
		`); err != nil {
			b.Fatal("Invalid StopFunc source:", err)
		}
		b.StartTimer()
		if err := inquiry.Start(); err != nil {
			b.Fatal("Error starting inquiry", err)
		}
		inquiry.Wait()
		b.StopTimer()
		if inquiry.Error() != nil {
			b.Fatal("Error running inquiry", inquiry.Error())
		}
		if len(tempReachedDids) > 0 {
			b.Fatal("Broadcast inquiry failed to reach", reachedDids)
		}
	}
	m.Wait()
}
