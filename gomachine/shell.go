package gomachine

import (
	"bufio"
	"fmt"
	"github.com/daviddengcn/go-colortext"
	"github.com/fernandosanchezjr/anko/ast"
	"github.com/fernandosanchezjr/anko/parser"
	"github.com/fernandosanchezjr/anko/vm"
	"github.com/mattn/go-isatty"
	"os"
	"strings"
)

type Shell struct {
	m      *Machine
	istty  bool
	source string
}

var sh *Shell

func NewShell(m *Machine) *Shell {
	if sh == nil {
		sh = &Shell{m: m, istty: isatty.IsTerminal(os.Stdout.Fd()), source: "shell"}
	}
	return sh
}

func (s *Shell) colortext(color ct.Color, bright bool, f func()) {
	if s.istty {
		ct.ChangeColor(color, bright, ct.None, false)
		f()
		ct.ResetColor()
	} else {
		f()
	}
}

func (s *Shell) Start() {
	go s.loop()
}

func (s *Shell) printPrompt(following bool) {
	s.colortext(ct.None, true, func() {
		if following {
			fmt.Fprint(os.Stdout, "  ")
		} else {
			fmt.Fprint(os.Stdout, "> ")
		}
	})
}

func (s *Shell) printValue(v interface{}) {
	s.colortext(ct.Green, true, func() {
		if v == nil {
			return
		} else {
			if s, ok := v.(fmt.Stringer); ok {
				fmt.Fprintln(os.Stdout, s)
			} else {
				fmt.Fprintf(os.Stdout, "%#v\n", v)
			}
		}
	})
}

func (s *Shell) printError(err error) {
	s.colortext(ct.Red, false, func() {
		if e, ok := err.(*vm.Error); ok {
			fmt.Fprintf(os.Stderr, "%s:%d: %s\n", s.source, e.Pos.Line, err)
		} else if e, ok := err.(*parser.Error); ok {
			if e.Filename != "" {
				s.source = e.Filename
			}
			fmt.Fprintf(os.Stderr, "%s:%d: %s\n", s.source, e.Pos.Line, err)
		} else {
			fmt.Fprintln(os.Stderr, err)
		}
	})
}

func (s *Shell) parseCode(code string, linelen int) ([]ast.Stmt, error, bool) {
	var following bool
	stmts, err := s.m.vm.Parse(code)
	if e, ok := err.(*parser.Error); ok {
		es := e.Error()
		if strings.HasPrefix(es, "syntax error: unexpected") {
			if strings.HasPrefix(es, "syntax error: unexpected $end,") {
				following = true
			}
		} else {
			if e.Pos.Column == linelen && !e.Fatal {
				s.colortext(ct.Red, false, func() {
					fmt.Fprintln(os.Stderr, e.Error())
				})
				following = true
			}
			if e.Error() == "unexpected EOF" {
				following = true
			}
		}
	}
	return stmts, err, following
}

func (s *Shell) loop() {
	var (
		code      string
		b         []byte
		reader    *bufio.Reader
		following bool
		stmts     []ast.Stmt
	)
	reader = bufio.NewReader(os.Stdin)
	for {
		s.printPrompt(following)
		var err error
		b, _, err = reader.ReadLine()
		if err != nil {
			break
		}
		if len(b) == 0 {
			continue
		}
		if code != "" {
			code += "\n"
		}
		code += string(b)
		if stmts, err, following = s.parseCode(code, len(b)); following {
			continue
		} else {
			following = false
			code = ""
			var v interface{} = nil
			if err == nil {
				v, err = s.m.vm.Run(stmts)
			}
			if err != nil {
				s.printError(err)
				continue
			} else {
				s.printValue(v)
			}
		}
	}
	s.m.exitchannel <- os.Interrupt
}
