package gomachine

import (
	"flag"
	"path"
	"sync"
	"time"
)

var machineRoot string = "/opt/gomachine2"
var cacheTTLMSec int = 100
var cacheTTL time.Duration = time.Duration(cacheTTLMSec) * time.Millisecond
var flagsMtx sync.Mutex
var machineName string
var shell bool = false
var httpSocket bool = false
var httpPort int = 9876
var httpAddress string = ""
var creationBeats bool = false

const defaultDomainSocketPath string = "/var/tmp"
const defaultDomainSocketExtension string = ".gomachine.sock"

var httpDomainSocket bool = false
var httpDomainSocketPath string = ""

func init() {
	flag.StringVar(&machineRoot, "machine_root", machineRoot, "Root folder")
	flag.IntVar(&cacheTTLMSec, "cache_ttl", cacheTTLMSec, "Local Bin ttl in msec")
	flag.StringVar(&machineName, "machine_name", "", "gomachine: name")
	flag.BoolVar(&shell, "shell", shell, "Enable shell")
	flag.BoolVar(&httpSocket, "http_socket", httpSocket, "Enable HTTP TCP socket")
	flag.IntVar(&httpPort, "http_socket_port", httpPort, "HTTP TCP socket port")
	flag.StringVar(&httpAddress, "http_socket_address", httpAddress, "HTTP TCP socket address")
	flag.BoolVar(&httpDomainSocket, "domain_socket", httpDomainSocket, "Enable HTTP UNIX domain socket")
	flag.StringVar(&httpDomainSocketPath, "domain_socket_path", httpDomainSocketPath, "HTTP UNIX domain socket path. "+
		"If not specified, defaults to "+path.Join(defaultDomainSocketPath, "<machine_name>"+
		defaultDomainSocketExtension))
	flag.BoolVar(&creationBeats, "creation_beats", creationBeats, "Enable creation beats")
}

func ParseFlags() {
	flagsMtx.Lock()
	if !flag.Parsed() {
		flag.Parse()
		cacheTTL = time.Duration(cacheTTLMSec) * time.Millisecond
		setupDomainSocketFlag()
	}
	flagsMtx.Unlock()
}

func setupDomainSocketFlag() {
	if httpDomainSocket && httpDomainSocketPath == "" && machineName != "" {
		httpDomainSocketPath = path.Join(defaultDomainSocketPath, machineName+defaultDomainSocketExtension)
	}
}

func GetMachineNameFlag() string {
	return machineName
}

func GetShellFlag() bool {
	return shell
}

func GetHTTPSocketEnabledFlag() bool {
	return httpSocket
}

func SetHTTPSocketEnabledFlag(value bool) {
	httpSocket = value
}

func GetHTTPSocketPortFlag() int {
	return httpPort
}

func SetHTTPSocketPortFlag(value int) {
	httpPort = value
}

func GetHTTPSocketAddressFlag() string {
	return httpAddress
}

func SetHTTPSocketAddressFlag(value string) {
	httpAddress = value
}

func GetHTTPDomainSocketEnabledFlag() bool {
	return httpDomainSocket
}

func SetHTTPDomainSocketEnabledFlag(value bool) {
	httpDomainSocket = value
}

func GetHTTPDomainSocketPathFlag() string {
	return httpDomainSocketPath
}

func SetHTTPDomainSocketPathFlag(value string) {
	httpDomainSocketPath = value
}
