package address

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"reflect"
	"strings"
)

type Did []interface{}

type DidSlice []Did

func ToDid(val []interface{}) Did {
	return (Did)(val)
}

func NewDid(val ...interface{}) Did {
	return val
}

func DidString(val []interface{}) string {
	return ToDid(val).String()
}

func (d Did) Length() int {
	return len(d)
}

func (d Did) Root() bool {
	return len(d) == 0
}

func ToString(value interface{}) string {
	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.String:
		return fmt.Sprintf("\"%s\"", value)
	case reflect.Struct:
		return fmt.Sprintf("%+#v", v.Interface())
	case reflect.Array, reflect.Slice:
		components := make([]string, v.Len())
		for i := 0; i < v.Len(); i++ {
			components[i] = ToString(v.Index(i).Interface())
		}
		return fmt.Sprintf("%s{%s}", v.Type(), strings.Join(components, ", "))
	case reflect.Ptr:
		return ToString(v.Elem().Interface())
	default:
		return fmt.Sprint(value)
	}
}

func (d Did) String() string {
	return ToString(([]interface{})(d))
}

func (d Did) Hash() []byte {
	hasher := sha256.New()
	hasher.Write([]byte(d.String()))
	return hasher.Sum(nil)
}

func (d Did) Path() string {
	hexChunks := make([]string, 0)
	didHash := d.Hash()
	var next []byte
	for {
		next, didHash = didHash[:1], didHash[1:]
		hexChunks = append(hexChunks, hex.EncodeToString(next))
		if len(didHash) == 0 {
			break
		}
	}
	return strings.Join(hexChunks, "/")
}

func (d Did) Equals(od Did) bool {
	if d.Length() != od.Length() {
		return false
	}
	return d.String() == od.String()
}

func (d Did) StartsWith(od Did) bool {
	if d.Length() < od.Length() {
		return false
	}
	for pos, value := range od {
		if value != d[pos] {
			return false
		}
	}
	return true
}

func (d Did) Append(id interface{}) Did {
	childDid := make([]interface{}, 0, len(d)+1)
	childDid = append(childDid, d...)
	return append(childDid, id)
}

func (d Did) Prepend(id interface{}) Did {
	childDid := make([]interface{}, 1, len(d)+1)
	childDid[0] = id
	return append(childDid, d...)
}

func (d Did) Id() interface{} {
	if didLen := len(d); didLen == 0 {
		return nil
	} else {
		return d[didLen-1]
	}
}

func (ds DidSlice) Contains(od Did) bool {
	for _, d := range ds {
		if d.Equals(od) {
			return true
		}
	}
	return false
}

func (ds DidSlice) StartWith(od Did) DidSlice {
	retDs := make(DidSlice, 0)
	for _, d := range ds {
		if d.StartsWith(od) {
			retDs = append(retDs, d)
		}
	}
	return retDs
}

func DidEquals(a, b Did) bool {
	return a.Equals(b)
}

func DidStartsWith(a, b Did) bool {
	return a.StartsWith(b)
}

func DidSliceContains(a Did, ds DidSlice) bool {
	return ds.Contains(a)
}

func DidSliceStartWith(a Did, ds DidSlice) DidSlice {
	return ds.StartWith(a)
}

func DidSliceLeft(d Did, l int) Did {
	return d[l:]
}

func DidSliceRight(d Did, r int) Did {
	return d[:r]
}

func DidSliceLeftRight(d Did, l int, r int) Did {
	return d[l:r]
}

func DidAppend(d Did, id interface{}) Did {
	return d.Append(id)
}

func DidPrepend(d Did, id interface{}) Did {
	return d.Prepend(id)
}
