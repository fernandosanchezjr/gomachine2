package gomachine

import (
	"fmt"
	"net/http"
)

func contentsHandler(ms *httpServer, w http.ResponseWriter, r *http.Request) {
	if !ms.checkStopped(w, r) {
		return
	}
	if !ms.checkMethod("POST", w, r) {
		return
	}
	if !ms.checkContentType("application/json", w, r) {
		return
	}
	if !ms.parseForm(w, r) {
		return
	}
	ip, err := newHttpInquiryParams(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error parsing request body: %s", err)))
		return
	}
	i := ms.newInquiry(ip.Did, ip.Depth, w, r)
	i.PutVMValue("newContentsResponse", newContentsResponse)
	if err := i.SetExecute(`
		_, err = response.WriteJSONChunk(newContentsResponse(bin))
		if err != nil {
			println(err)
			inquiry.Stop(err)
		}
	`); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error setting inquiry Execute: %s", err)))
	}
	w.Header().Set("Content-Type", "application/json")
	err = i.Start()
	i.Wait()
	if !ms.checkInquiryResult(i, w, r) {
		return
	}
}
