package gomachine

import (
	"encoding/json"
	"gomachine/address"
	"io"
)

type httpInquiryParams struct {
	Did   address.Did `json:"did"`
	Depth int         `json:"depth"`
}

type httpContentsResponse struct {
	Id       interface{}            `json:"id"`
	Did      address.Did            `json:"did"`
	Terminal bool                   `json:"terminal"`
	Children []interface{}          `json:"children"`
	Data     map[string]interface{} `json:"data"`
	Meta     map[string]interface{} `json:"meta"`
	Beats    map[string]*VMCode     `json:"beats"`
}

type httpPutDataParams struct {
	Did   address.Did `json:"did"`
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

type httpGetDataParams struct {
	Did address.Did `json:"did"`
	Key string      `json:"key"`
}

type httpGetDataResponse struct {
	Value interface{} `json:"value"`
	Found bool        `json:"found"`
}

type httpPutBeatParams struct {
	Did   address.Did `json:"did"`
	Key   string      `json:"key"`
	Value string      `json:"value"`
}

type httpExecuteBeatParams struct {
	Did    address.Did   `json:"did"`
	Beat   string        `json:"did"`
	Params []interface{} `json:"params"`
}

func newHttpInquiryParams(r io.Reader) (*httpInquiryParams, error) {
	hip := &httpInquiryParams{}
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(hip); err != nil {
		return nil, err
	}
	return hip, nil
}

func newContentsResponse(bin Bin) *httpContentsResponse {
	bin.StartOperation()
	defer bin.OperationComplete()
	cr := &httpContentsResponse{Id: bin.Id(), Did: bin.Did(), Terminal: bin.Terminal(), Children: bin.ChildIds(),
		Data: make(map[string]interface{}), Meta: make(map[string]interface{}), Beats: make(map[string]*VMCode)}
	for _, key := range bin.GetKeys() {
		value, _ := bin.Get(key)
		cr.Data[key] = value
	}
	for _, key := range bin.GetMetaKeys() {
		value, _ := bin.GetMeta(key)
		cr.Meta[key] = value
	}
	for _, key := range bin.GetBeatNames() {
		value, _ := bin.GetBeat(key)
		cr.Beats[key] = value
	}
	return cr
}

func newHttpPutDataParams(r io.Reader) (*httpPutDataParams, error) {
	hpp := &httpPutDataParams{}
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(hpp); err != nil {
		return nil, err
	}
	return hpp, nil
}

func newHttpGetDataParams(r io.Reader) (*httpGetDataParams, error) {
	hgp := &httpGetDataParams{}
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(hgp); err != nil {
		return nil, err
	}
	return hgp, nil
}

func newHttpGetDataResponse(value interface{}, found bool) *httpGetDataResponse {
	return &httpGetDataResponse{Value: value, Found: found}
}

func newHttpPutBeatParams(r io.Reader) (*httpPutBeatParams, error) {
	hpp := &httpPutBeatParams{}
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(hpp); err != nil {
		return nil, err
	}
	return hpp, nil
}

func newHttpExecuteBeatParams(r io.Reader) (*httpExecuteBeatParams, error) {
	hebp := &httpExecuteBeatParams{}
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(hebp); err != nil {
		return nil, err
	}
	return hebp, nil
}
