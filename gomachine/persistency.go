package gomachine

import (
	"bytes"
	"encoding/gob"
	"gomachine/address"
)

type binData struct {
	ChildCount int
	DataCount  int
	MetaCount  int
	BeatsCount int
	Data       map[string]interface{}
	Meta       map[string]interface{}
	Beats      map[string]*VMCode
	Children   map[string]address.Did
}

func newBinData(bin *baseBin) *binData {
	bd := &binData{ChildCount: bin.childCount, DataCount: bin.dataCount, MetaCount: bin.metaCount,
		BeatsCount: bin.beatsCount, Data: bin.data, Meta: bin.meta, Beats: bin.beats,
		Children: bin.children}
	return bd
}

func (bd *binData) serialize(buf *bytes.Buffer) error {
	encoder := gob.NewEncoder(buf)
	return encoder.Encode(bd)
}

func (bd *binData) deserialize(buf *bytes.Buffer) error {
	encoder := gob.NewDecoder(buf)
	return encoder.Decode(bd)
}
