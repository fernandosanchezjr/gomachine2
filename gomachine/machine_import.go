package gomachine

import (
	"github.com/fernandosanchezjr/anko/vm"
	"gomachine/address"
	"gomachine/strategies"
	"reflect"
)

func machineImport(env *vm.Env) *vm.Env {
	m := env.NewPackage("gomachine")
	m.Define("ToDid", address.ToDid)
	m.Define("NewDid", address.NewDid)
	m.Define("DidString", address.DidString)
	m.Define("DidEquals", address.DidEquals)
	m.Define("DidStartsWith", address.DidStartsWith)
	m.Define("DidSliceContains", address.DidSliceContains)
	m.Define("DidSliceStartWith", address.DidSliceStartWith)
	m.Define("DidSliceLeft", address.DidSliceLeft)
	m.Define("DidSliceRight", address.DidSliceRight)
	m.Define("DidSliceLeftRight", address.DidSliceLeftRight)
	m.Define("DidAppend", address.DidAppend)
	m.Define("DidPrepend", address.DidPrepend)
	var d address.Did
	m.DefineType("Did", reflect.TypeOf(d))
	return m
}

func strategyImport(env *vm.Env) *vm.Env {
	m := env.NewPackage("strategies")
	m.Define("UByte", strategies.UByte)
	m.Define("UShort", strategies.UShort)
	m.Define("UInt", strategies.UInt)
	m.Define("ULong", strategies.ULong)
	m.Define("Number", strategies.Number)
	m.Define("Join", strategies.Join)

	return m
}
