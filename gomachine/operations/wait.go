package operations

import (
	"sync"
)

type Wait struct {
	mutex   sync.Mutex
	entries []chan int
}

func NewWait() *Wait {
	return &Wait{entries: make([]chan int, 0)}
}

func (w *Wait) Notify() {
	w.mutex.Lock()
	defer w.mutex.Unlock()
	for _, e := range w.entries {
		close(e)
	}
	w.entries = make([]chan int, 0)
}

func (w *Wait) register() chan int {
	w.mutex.Lock()
	defer w.mutex.Unlock()
	entry := make(chan int)
	w.entries = append(w.entries, entry)
	return entry
}

func (w *Wait) Wait() {
	entry := w.register()
	select {
	case <-entry:
		return
	}
}
