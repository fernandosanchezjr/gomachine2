package operations

import (
	"sync/atomic"
)

type Count struct {
	opcount    uint64
	onComplete func()
}

func NewCount(onComplete func()) *Count {
	oc := &Count{onComplete: onComplete}
	return oc
}

func (oc *Count) Busy() bool {
	return atomic.LoadUint64(&oc.opcount) > 0
}

func (oc *Count) StartOperation() {
	atomic.AddUint64(&oc.opcount, 1)
}

func (oc *Count) OperationComplete() {
	if atomic.AddUint64(&oc.opcount, ^uint64(0)) == 0 {
		if oc.onComplete != nil {
			(oc.onComplete)()
		}
	}
}
