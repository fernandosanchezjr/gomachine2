package operations

type Busy interface {
	Busy() bool
}

type OperationTracker interface {
	StartOperation()
	OperationComplete()
}

type OperationsCompleteMonitor interface {
	OnOperationsComplete()
}
