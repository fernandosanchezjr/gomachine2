package gomachine

import ()

type currentBin struct {
	Bin
	retrievedBins map[interface{}]Bin
}

func newCurrentBin(b Bin) *currentBin {
	return &currentBin{Bin: b, retrievedBins: make(map[interface{}]Bin)}
}

func (c *currentBin) Execute(source string) (interface{}, error) {
	go c.Bin.Execute(source)
	return nil, nil
}

func (c *currentBin) Beat(name string, args ...interface{}) (interface{}, error) {
	go c.Bin.Beat(name, args...)
	return nil, nil
}

func (c *currentBin) Broadcast(bcast Broadcast) {
	go c.Bin.Broadcast(bcast)
}

func (c *currentBin) Wait() {

}

func (c *currentBin) Parent() Bin {
	b := c.Bin.Parent()
	if _, ok := c.retrievedBins[b.Id()]; !ok {
		b.StartOperation()
		c.retrievedBins[b.Id()] = b
	}
	return b
}

func (c *currentBin) Children() map[interface{}]Bin {
	retMap := c.Bin.Children()
	for id, b := range retMap {
		if _, ok := c.retrievedBins[id]; !ok {
			b.StartOperation()
			c.retrievedBins[id] = b
		}
	}
	return retMap
}

func (c *currentBin) AddChild(id interface{}) Bin {
	b := c.Bin.AddChild(id)
	if _, ok := c.retrievedBins[id]; !ok {
		b.StartOperation()
		c.retrievedBins[id] = b
	}
	return b
}

func (c *currentBin) GetChild(id interface{}) (Bin, bool) {
	b, found := c.Bin.GetChild(id)
	if found {
		if _, ok := c.retrievedBins[id]; !ok {
			b.StartOperation()
			c.retrievedBins[b.Id()] = b
		}
	}
	return b, found
}

func (c *currentBin) RemoveChild(id interface{}) {
	if b, ok := c.retrievedBins[id]; ok {
		delete(c.retrievedBins, id)
		b.OperationComplete()
	}
	c.Bin.RemoveChild(id)
}

func (c *currentBin) cleanup() {
	for _, b := range c.retrievedBins {
		b.OperationComplete()
	}
}
