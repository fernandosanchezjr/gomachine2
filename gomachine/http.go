package gomachine

import (
	"fmt"
	"gomachine/address"
	"gomachine/ioutil"
	"net"
	"net/http"
	"sync"
)

type httpServer struct {
	stopped         bool
	m               *Machine
	tcpSocket       net.Listener
	tcpSocketMtx    sync.Mutex
	domainSocket    net.Listener
	domainSocketMtx sync.Mutex
	mux             *http.ServeMux
}

func newMachineHTTPServer(m *Machine) *httpServer {
	s := &httpServer{m: m, mux: http.NewServeMux()}
	s.setupSocket()
	s.setupDomainSocket()
	s.setupDefaultHandlers()
	return s
}

func (ms *httpServer) setupDefaultHandlers() {
	ms.mux.HandleFunc("/machine/contents", MachineViewFuncWrapper(ms, contentsHandler))
	ms.mux.HandleFunc("/machine/data", MachineViewFuncWrapper(ms, dataHandler))
	ms.mux.HandleFunc("/machine/meta", MachineViewFuncWrapper(ms, metaHandler))
	ms.mux.HandleFunc("/machine/beats", MachineViewFuncWrapper(ms, binBeatHandler))
	ms.mux.HandleFunc("/machine/beats/execute", MachineViewFuncWrapper(ms, binExecutionHandler))
}

func (ms *httpServer) setupSocket() {
	ms.tcpSocketMtx.Lock()
	defer ms.tcpSocketMtx.Unlock()
	if GetHTTPSocketEnabledFlag() {
		if l, err := net.Listen("tcp", fmt.Sprintf("%s:%d", GetHTTPSocketAddressFlag(),
			GetHTTPSocketPortFlag())); err != nil {
			panic(err)
		} else {
			ms.tcpSocket = l
		}
	}
}

func (ms *httpServer) closeSocket() {
	ms.tcpSocketMtx.Lock()
	defer ms.tcpSocketMtx.Unlock()
	if ms.tcpSocket != nil {
		ms.tcpSocket.Close()
		ms.tcpSocket = nil
	}
}

func (ms *httpServer) setupDomainSocket() {
	ms.domainSocketMtx.Lock()
	defer ms.domainSocketMtx.Unlock()
	if GetHTTPDomainSocketEnabledFlag() {
		ioutil.DeleteFile(GetHTTPDomainSocketPathFlag())
		if l, err := net.Listen("unix", GetHTTPDomainSocketPathFlag()); err != nil {
			panic(err)
		} else {
			ms.domainSocket = l
		}
	}
}

func (ms *httpServer) closeDomainSocket() {
	ms.domainSocketMtx.Lock()
	defer ms.domainSocketMtx.Unlock()
	if ms.domainSocket != nil {
		ms.domainSocket.Close()
		ms.domainSocket = nil
	}
}

func (ms *httpServer) serveSocket() {
	if ms.tcpSocket != nil {
		defer ms.closeSocket()
		http.Serve(ms.tcpSocket, ms.mux)
	}
}

func (ms *httpServer) serveDomainSocket() {
	if ms.domainSocket != nil {
		defer ms.closeDomainSocket()
		http.Serve(ms.domainSocket, ms.mux)
	}
}

func (ms *httpServer) Serve() {
	go ms.serveSocket()
	go ms.serveDomainSocket()
}

func (ms *httpServer) Handle(pattern string, handler http.Handler) {
	ms.mux.Handle(pattern, handler)
}

func (ms *httpServer) HandleFunc(pattern string, handler http.HandlerFunc) {
	ms.mux.HandleFunc(pattern, handler)
}

func (ms *httpServer) stop() {
	ms.stopped = true
}

func (ms *httpServer) newInquiry(did address.Did, depth int, w http.ResponseWriter, r *http.Request) Inquiry {
	i := ms.m.NewInquiry(did, depth)
	i.PutVMValue("response", NewResponseWriter(w))
	i.PutVMValue("request", NewRequest(r))
	return i
}

func (ms *httpServer) checkStopped(w http.ResponseWriter, r *http.Request) bool {
	if ms.stopped {
		w.WriteHeader(http.StatusGone)
		return false
	}
	return true
}

func (ms *httpServer) checkMethod(m string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != m {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte(fmt.Sprintf("Method %s not allowed", r.Method)))
		return false
	}
	return true
}

func (ms *httpServer) checkMethods(m []string, w http.ResponseWriter, r *http.Request) bool {
	var found bool
	for _, cm := range m {
		if cm == r.Method {
			found = true
		}
	}
	if !found {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte(fmt.Sprintf("Method %s not allowed", r.Method)))
		return false
	}
	return true
}

func (ms *httpServer) checkContentType(ct string, w http.ResponseWriter, r *http.Request) bool {
	if r.Header.Get("Content-Type") != ct {
		w.WriteHeader(http.StatusNotAcceptable)
		w.Write([]byte("Content-Type must be application/json"))
		return false
	}
	return true
}

func (ms *httpServer) parseForm(w http.ResponseWriter, r *http.Request) bool {
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error parsing form: %s", err)))
		return false
	}
	return true
}

func (ms *httpServer) checkInquiryResult(i Inquiry, w http.ResponseWriter, r *http.Request) bool {
	if i.Error() != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error during inqury: %s", i.Error())))
		return false
	}
	return true
}
