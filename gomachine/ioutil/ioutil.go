package ioutil

import (
	"io/ioutil"
	"math/rand"
	"os"
	"sync"
	"time"
)

var defaultPerm os.FileMode = 0750
var pathMutex sync.Mutex

func CreatePath(p string) {
	pathMutex.Lock()
	defer pathMutex.Unlock()
	err := os.MkdirAll(p, defaultPerm)
	if err != nil {
		panic(err)
	}
}

func DeletePath(p string) {
	//pathMutex.Lock()
	//defer pathMutex.Unlock()
	err := os.RemoveAll(p)
	if err != nil {
		panic(err)
	}
}

func DeleteFile(p string) {
	//pathMutex.Lock()
	//defer pathMutex.Unlock()
	os.Remove(p)
}

func CreateFile(path string) *os.File {
	//pathMutex.Lock()
	//defer pathMutex.Unlock()
	vfile, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, defaultPerm)
	if err != nil {
		panic(err)
	}
	return vfile
}

func CreateFileExclusive(path string) *os.File {
	//pathMutex.Lock()
	//defer pathMutex.Unlock()
	vfile, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_EXCL, defaultPerm)
	if err != nil {
		panic(err)
	}
	return vfile
}

func PathExists(path string) bool {
	//pathMutex.Lock()
	//defer pathMutex.Unlock()
	_, err := os.Stat(path)
	if err != nil {
		return false
	}
	return true
}

func ReadFile(path string) ([]byte, error) {
	//pathMutex.Lock()
	//defer pathMutex.Unlock()
	return ioutil.ReadFile(path)
}

func Random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
