package gomachine

import (
	"reflect"
	"testing"
)

func TestDidWalk(t *testing.T) {
	m := createTestMachine("TestDidWalk")
	bin := m.DidWalk([]interface{}{})
	if bin != m.Bin {
		t.Fatal("Blank Did does not return root bin")
	}
	bin = m.DidWalk(bin123Did)
	if !reflect.DeepEqual(bin.Did(), bin123Did) {
		t.Fatal("Invalid bin Did: got", bin.Did(), " - expected", bin123Did)
	}
	m.Wait()
}

func TestGetPutDelete(t *testing.T) {
	m := createTestMachine("TestGetPutDelete")
	m.DirectPut(bin123Did, intKey, intValue)
	value, found := m.DirectGet(bin123Did, intKey)
	if !found {
		t.Fatal("Value set in", bin123Did, "not found")
	}
	if value != intValue {
		t.Fatal("Value retrieved from", bin123Did, "does not match value set: got", value, "- expected", intValue)
	}
	m.DirectDelete(bin123Did, intKey)
	_, found = m.DirectGet(bin123Did, intKey)
	if found {
		t.Fatal("Value deleted in", bin123Did, "still found")
	}
	m.DirectPut(bin123Did, intKey, intValue)
	value, found = m.DirectGet(bin123Did, intKey)
	if !found {
		t.Fatal("Value set in", bin123Did, "not found")
	}
	if value != intValue {
		t.Fatal("Value retrieved from", bin123Did, "does not match value set: got", value, "- expected", intValue)
	}
	m.DirectPut(bin123Did, strKey, strValue)
	keys := m.DirectGetKeys(bin123Did)
	for _, k := range dataKeys {
		if found, _ := stringArrayContains(k, keys); !found {
			t.Fatal("Key", k, "not found in", bin123Did)
		}
	}
	m.Wait()
}

func TestGetPutDeleteMeta(t *testing.T) {
	m := createTestMachine("TestGetPutDeleteMeta")
	m.DirectPutMeta(bin123Did, intKey, intValue)
	value, found := m.DirectGetMeta(bin123Did, intKey)
	if !found {
		t.Fatal("Meta value set in", bin123Did, "not found")
	}
	if value != intValue {
		t.Fatal("Meta value retrieved from", bin123Did, "does not match value set: got", value, "- expected", intValue)
	}
	m.DirectDeleteMeta(bin123Did, intKey)
	_, found = m.DirectGetMeta(bin123Did, intKey)
	if found {
		t.Fatal("Meta value deleted in", bin123Did, "still found")
	}
	m.DirectPutMeta(bin123Did, intKey, intValue)
	value, found = m.DirectGetMeta(bin123Did, intKey)
	if !found {
		t.Fatal("Meta value set in", bin123Did, "not found")
	}
	if value != intValue {
		t.Fatal("Meta value retrieved from", bin123Did, "does not match value set: got", value, "- expected", intValue)
	}
	m.DirectPutMeta(bin123Did, strKey, strValue)
	keys := m.DirectGetMetaKeys(bin123Did)
	for _, k := range dataKeys {
		if found, _ := stringArrayContains(k, keys); !found {
			t.Fatal("Meta key", k, "not found in", bin123Did)
		}
	}
	m.Wait()
}
