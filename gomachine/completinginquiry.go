package gomachine

import ()

type deferredAccumulate struct {
	parent bool
	data   interface{}
}

type completingInquiry struct {
	*vmInquiry
	accumulates []*deferredAccumulate
}

func newCompletingInquiry(i *vmInquiry) *completingInquiry {
	return &completingInquiry{vmInquiry: i, accumulates: make([]*deferredAccumulate, 0)}
}

func (vi *completingInquiry) AccumulateToParent(data interface{}) {
	vi.accumulates = append(vi.accumulates, &deferredAccumulate{parent: true, data: data})
}

func (vi *completingInquiry) AccumulateToMaster(data interface{}) {
	vi.accumulates = append(vi.accumulates, &deferredAccumulate{parent: false, data: data})
}
