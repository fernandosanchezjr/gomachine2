package gomachine

import (
	"encoding/json"
	"github.com/fernandosanchezjr/anko/ast"
	"github.com/fernandosanchezjr/anko/vm"
)

type VMCode struct {
	AST []ast.Stmt
	Raw string
}

func newVMCode(ast []ast.Stmt, raw string) *VMCode {
	return &VMCode{AST: ast, Raw: raw}
}

func stringToVMCode(vm *ankoVM, raw string) (*VMCode, error) {
	if ast, err := vm.Parse(raw); err != nil {
		return nil, err
	} else {
		return newVMCode(ast, raw), nil
	}
}

func stringToVMCodeFunc(vm *ankoVM, raw string) (*VMCode, vm.Func, error) {
	vmc, err := stringToVMCode(vm, raw)
	if err != nil {
		return nil, nil, err
	}
	vmf, err := vmc.InterpretFunction(vm)
	return vmc, vmf, nil
}

func (v *VMCode) InterpretFunction(vm *ankoVM) (vm.Func, error) {
	if vmf, err := vm.InterpretFunction(v.AST); err != nil {
		return nil, err
	} else {
		return vmf, nil
	}
}

func (v *VMCode) String() string {
	return v.Raw
}

func (v *VMCode) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.Raw)
}
