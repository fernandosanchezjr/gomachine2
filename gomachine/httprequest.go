package gomachine

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"sync"
)

type ResponseWriter struct {
	contentType string
	http.ResponseWriter
	mtx sync.Mutex
}

type Request struct {
	*http.Request
	mtx sync.Mutex
}

func NewResponseWriter(w http.ResponseWriter) *ResponseWriter {
	return &ResponseWriter{ResponseWriter: w}
}

func NewRequest(r *http.Request) *Request {
	return &Request{Request: r}
}

func (w *ResponseWriter) Header() http.Header {
	w.mtx.Lock()
	defer w.mtx.Unlock()
	return w.ResponseWriter.Header()
}

func (w *ResponseWriter) Write(b []byte) (int, error) {
	w.mtx.Lock()
	defer w.mtx.Unlock()
	return w.ResponseWriter.Write(b)
}

func (w *ResponseWriter) WriteString(s string) (int, error) {
	return w.Write([]byte(s))
}

func (w *ResponseWriter) WriteJSON(i interface{}) (int, error) {
	var buf bytes.Buffer
	w.SetContentType("application/json")
	encoder := json.NewEncoder(&buf)
	if err := encoder.Encode(i); err != nil {
		return 0, err
	}
	return w.Write(buf.Bytes())
}

func (w *ResponseWriter) WriteHeader(h int) {
	w.mtx.Lock()
	defer w.mtx.Unlock()
	w.ResponseWriter.WriteHeader(h)
}

func (w *ResponseWriter) WriteChunk(b []byte) (int, error) {
	w.mtx.Lock()
	defer w.mtx.Unlock()
	f, ok := w.ResponseWriter.(http.Flusher)
	if !ok {
		return 0, fmt.Errorf("ResponseWriter is not a valid Flusher()")
	}
	c, err := w.ResponseWriter.Write(b)
	if err == nil {
		f.Flush()
	}
	return c, err
}

func (w *ResponseWriter) WriteStringChunk(s string) (int, error) {
	return w.WriteChunk([]byte(s))
}

func (w *ResponseWriter) WriteJSONChunk(i interface{}) (int, error) {
	var buf bytes.Buffer
	w.SetContentType("application/json")
	encoder := json.NewEncoder(&buf)
	if err := encoder.Encode(i); err != nil {
		return 0, err
	}
	return w.WriteChunk(buf.Bytes())
}

func (w *ResponseWriter) AddHeader(k, v string) {
	w.Header().Add(k, v)
	if k == "Content-Type" {
		w.contentType = v
	}
}

func (w *ResponseWriter) SetContentType(t string) {
	if w.contentType != "" {
		return
	}
	w.Header().Add("Content-Type", t)
}

func (r *Request) AddCookie(c *http.Cookie) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	r.Request.AddCookie(c)
}

func (r *Request) BasicAuth() (string, string, bool) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.BasicAuth()
}

func (r *Request) Cookie(name string) (*http.Cookie, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.Cookie(name)
}

func (r *Request) Cookies() []*http.Cookie {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.Cookies()
}

func (r *Request) FormFile(key string) (multipart.File, *multipart.FileHeader, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.FormFile(key)
}

func (r *Request) FormValue(key string) string {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.FormValue(key)
}

func (r *Request) MultipartReader() (*multipart.Reader, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.MultipartReader()
}

func (r *Request) ParseForm() error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.ParseForm()
}

func (r *Request) ParseMultipartForm(maxMemory int64) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.ParseMultipartForm(maxMemory)
}

func (r *Request) PostFormValue(key string) string {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.PostFormValue(key)
}

func (r *Request) ProtoAtLeast(major, minor int) bool {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.ProtoAtLeast(major, minor)
}

func (r *Request) Referer() string {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.Referer()
}

func (r *Request) SetBasicAuth(username, password string) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	r.Request.SetBasicAuth(username, password)
}

func (r *Request) UserAgent() string {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.UserAgent()
}

func (r *Request) Write(w io.Writer) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.Write(w)
}

func (r *Request) WriteProxy(w io.Writer) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.Request.WriteProxy(w)
}
