package gomachine

import (
	"gomachine/address"
	"gomachine/operations"
	"time"
)

type BusyAware interface {
	Busy() bool
}

type BusyBroadcaster interface {
	BusyAware
	Wait()
}

type OperationCompleter interface {
	operationsComplete(time.Time)
}

type OperationHost interface {
	operations.OperationsCompleteMonitor
	StartOperation()
	OperationComplete()
}

type DataHolder interface {
	Put(string, interface{})
	GetKeys() []string
	Get(string) (interface{}, bool)
	Delete(string)
	ClearData()
}

type MetaHolder interface {
	PutMeta(string, interface{})
	GetMetaKeys() []string
	GetMeta(string) (interface{}, bool)
	DeleteMeta(string)
	ClearMeta()
}

type BeatsHolder interface {
	PutBeat(string, string) error
	GetBeat(string) (*VMCode, bool)
	DeleteBeat(string)
	GetBeatNames() []string
	ClearBeats()
}

type BinHolder interface {
	Empty() bool
	Children() map[interface{}]Bin
	ChildIds() []interface{}
	ChildCount() int
	AddChild(interface{}) Bin
	GetChild(interface{}) (Bin, bool)
	RemoveChild(interface{})
}

type Bin interface {
	BusyAware
	OperationHost
	BinHolder
	DataHolder
	MetaHolder
	BeatsHolder
	String() string
	Machine() *Machine
	Did() address.Did
	Path() string
	Terminal() bool
	Id() interface{}
	Root() bool
	Parent() Bin
	Changed() bool
	Lock()
	Unlock()
	disowned() bool
	disown()
	Persist()
	Unpersist()
	Execute(string) (interface{}, error)
	Beat(string, ...interface{}) (interface{}, error)
	ContextBeat(string, map[string]interface{}, ...interface{}) (interface{}, error)
	Broadcast(Broadcast)
	Clear()
}

type Broadcast interface {
	BusyBroadcaster
	OperationHost
	Did() address.Did
	Machine() *Machine
	Depth() int
	SetDepth(int)
	stop(Bin) bool
	execute(Bin)
	clone() Broadcast
	spawn() Broadcast
	Start() error
	broadcastStep(Bin)
}

type Inquiry interface {
	Broadcast
	DataHolder
	Root() bool
	Stopped() bool
	Stop(error)
	GetStop() *VMCode
	SetStop(f string) error
	GetExecute() *VMCode
	SetExecute(f string) error
	GetVMValue(string) (interface{}, bool)
	PutVMValue(string, interface{})
	AccumulateToParent(interface{})
	AccumulateToMaster(interface{})
	accumulate(address.Did, interface{})
	GetAccumulate() *VMCode
	SetAccumulate(string) error
	GetComplete() *VMCode
	SetComplete(string) error
	complete()
	Error() error
}
