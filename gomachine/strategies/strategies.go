package strategies

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gomachine/address"
	"path"
	"reflect"
)

func ByteArrayToDid(b []byte) address.Did {
	ret := make(address.Did, len(b))
	for pos, bs := range b {
		ret[pos] = fmt.Sprintf("%02x", bs)
	}
	return ret
}

func ByteArrayToPath(b []byte) string {
	ret := make([]string, len(b))
	for pos, bs := range b {
		ret[pos] = fmt.Sprintf("%02x", bs)
	}
	return path.Join(ret...)
}

func UByte(b byte) address.Did {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, b)
	return ByteArrayToDid(buf.Bytes())
}

func UShort(s uint16) address.Did {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, s)
	return ByteArrayToDid(buf.Bytes())
}

func UInt(u uint32) address.Did {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, u)
	return ByteArrayToDid(buf.Bytes())
}

func ULong(l uint64) address.Did {
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, l)
	return ByteArrayToDid(buf.Bytes())
}

func Number(n interface{}) address.Did {
	switch n := n.(type) {
	case byte:
		return UByte(n)
	case int8:
		return UByte(byte(n))
	case int16:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		default:
			return UShort(uint16(n))
		}
	case uint16:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		default:
			return UShort(uint16(n))
		}
	case int:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		case n < 0x10000:
			return UShort(uint16(n))
		default:
			return UInt(uint32(n))
		}
	case int32:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		case n < 0x10000:
			return UShort(uint16(n))
		default:
			return UInt(uint32(n))
		}
	case uint:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		case n < 0x10000:
			return UShort(uint16(n))
		default:
			return UInt(uint32(n))
		}
	case uint32:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		case n < 0x10000:
			return UShort(uint16(n))
		default:
			return UInt(uint32(n))
		}
	case int64:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		case n < 0x10000:
			return UShort(uint16(n))
		case n < 0x100000000:
			return UInt(uint32(n))
		default:
			return ULong(uint64(n))
		}
	case uint64:
		switch {
		case n < 0x100:
			return UByte(byte(n))
		case n < 0x10000:
			return UShort(uint16(n))
		case n < 0x100000000:
			return UInt(uint32(n))
		default:
			return ULong(n)
		}
	default:
		panic(fmt.Errorf("Invalid type for %s", n))
	}
}

func Join(d address.Did, i interface{}) address.Did {
	iv := reflect.ValueOf(i)
	ret := make(address.Did, len(d)+iv.Len())
	pos := 0
	for _, id := range d {
		ret[pos] = id
		pos = pos + 1
	}
	for i := 0; i < iv.Len(); i++ {
		ret[pos] = iv.Index(i).Interface()
		pos = pos + 1
	}
	return ret
}
