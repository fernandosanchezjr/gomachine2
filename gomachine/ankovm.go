package gomachine

import (
	"github.com/fernandosanchezjr/anko/ast"
	"github.com/fernandosanchezjr/anko/parser"
	"github.com/fernandosanchezjr/anko/vm"

	anko_core "github.com/fernandosanchezjr/anko/builtins"
	anko_encoding_json "github.com/fernandosanchezjr/anko/builtins/encoding/json"
	anko_flag "github.com/fernandosanchezjr/anko/builtins/flag"
	anko_fmt "github.com/fernandosanchezjr/anko/builtins/fmt"
	anko_colortext "github.com/fernandosanchezjr/anko/builtins/github.com/daviddengcn/go-colortext"
	anko_io "github.com/fernandosanchezjr/anko/builtins/io"
	anko_io_ioutil "github.com/fernandosanchezjr/anko/builtins/io/ioutil"
	anko_math "github.com/fernandosanchezjr/anko/builtins/math"
	anko_math_rand "github.com/fernandosanchezjr/anko/builtins/math/rand"
	anko_net "github.com/fernandosanchezjr/anko/builtins/net"
	anko_net_http "github.com/fernandosanchezjr/anko/builtins/net/http"
	anko_net_url "github.com/fernandosanchezjr/anko/builtins/net/url"
	anko_os "github.com/fernandosanchezjr/anko/builtins/os"
	anko_os_exec "github.com/fernandosanchezjr/anko/builtins/os/exec"
	anko_os_signal "github.com/fernandosanchezjr/anko/builtins/os/signal"
	anko_path "github.com/fernandosanchezjr/anko/builtins/path"
	anko_path_filepath "github.com/fernandosanchezjr/anko/builtins/path/filepath"
	anko_regexp "github.com/fernandosanchezjr/anko/builtins/regexp"
	anko_runtime "github.com/fernandosanchezjr/anko/builtins/runtime"
	anko_sort "github.com/fernandosanchezjr/anko/builtins/sort"
	anko_strings "github.com/fernandosanchezjr/anko/builtins/strings"
	anko_time "github.com/fernandosanchezjr/anko/builtins/time"

	"fmt"
	"gomachine/ioutil"
	"os"
	"reflect"
	"sync"
)

var nonFuncError error = fmt.Errorf("Statements do not produce a function")

type ankoVM struct {
	env       *vm.Env
	mtx       sync.Mutex
	importMtx sync.Mutex
	pkgs      map[string]func(env *vm.Env) *vm.Env
}

func newAnkoVM() *ankoVM {
	avm := &ankoVM{env: vm.NewEnv()}
	avm.init()
	return avm
}

func (avm *ankoVM) setupDefaultImports() {
	avm.pkgs = map[string]func(env *vm.Env) *vm.Env{
		"encoding/json": anko_encoding_json.Import,
		"flag":          anko_flag.Import,
		"fmt":           anko_fmt.Import,
		"io":            anko_io.Import,
		"io/ioutil":     anko_io_ioutil.Import,
		"math":          anko_math.Import,
		"math/rand":     anko_math_rand.Import,
		"net":           anko_net.Import,
		"net/http":      anko_net_http.Import,
		"net/url":       anko_net_url.Import,
		"os":            anko_os.Import,
		"os/exec":       anko_os_exec.Import,
		"os/signal":     anko_os_signal.Import,
		"path":          anko_path.Import,
		"path/filepath": anko_path_filepath.Import,
		"regexp":        anko_regexp.Import,
		"runtime":       anko_runtime.Import,
		"sort":          anko_sort.Import,
		"strings":       anko_strings.Import,
		"time":          anko_time.Import,
		"github.com/daviddengcn/go-colortext": anko_colortext.Import,
	}
}

func (avm *ankoVM) init() {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	avm.setupDefaultImports()
	anko_core.Import(avm.env)
	avm.env.Define("args", os.Args)
	avm.env.Define("import", func(s string) interface{} {
		avm.importMtx.Lock()
		defer avm.importMtx.Unlock()
		if loader, ok := avm.pkgs[s]; ok {
			m := loader(avm.env)
			return m
		}
		panic(fmt.Sprintf("package '%s' not found", s))
	})
}

func (avm *ankoVM) SetImport(name string, loader func(*vm.Env) *vm.Env) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	avm.pkgs[name] = loader
	avm.env.Define(name, loader(avm.env))
}

func (avm *ankoVM) Get(key string) (interface{}, error) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	if value, err := avm.env.Get(key); err != nil {
		return nil, err
	} else {
		return value.Interface(), nil
	}
}

func (avm *ankoVM) Set(key string, value interface{}) error {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return avm.env.Set(key, value)
}

func (avm *ankoVM) Addr(key string) (interface{}, error) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	if value, err := avm.env.Addr(key); err != nil {
		return nil, err
	} else {
		return value.Interface(), nil
	}
}

func (avm *ankoVM) Define(key string, value interface{}) error {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return avm.env.Define(key, value)
}

func (avm *ankoVM) DefineGlobal(key string, value interface{}) error {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return avm.env.DefineGlobal(key, value)
}

func (avm *ankoVM) DefineType(key string, value interface{}) error {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return avm.env.DefineType(key, value)
}

func (avm *ankoVM) Destroy() {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	avm.env.Destroy()
}

func (avm *ankoVM) Dump() {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	avm.env.Dump()
}

func (avm *ankoVM) Type(key string) (reflect.Type, error) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return avm.env.Type(key)
}

func (avm *ankoVM) SetName(name string) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	avm.env.SetName(name)
}

func (avm *ankoVM) String() string {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return avm.env.String()
}

func (avm *ankoVM) NewChildEnv() *ankoVM {
	return &ankoVM{env: avm.env.NewEnv()}
}

func (avm *ankoVM) Execute(source string) (interface{}, error) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	if value, err := avm.env.Execute(source); err != nil {
		return nil, err
	} else {
		return value.Interface(), nil
	}
}

func (avm *ankoVM) Parse(source string) ([]ast.Stmt, error) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	return parser.ParseSrc(source)
}

func (avm *ankoVM) Run(statements []ast.Stmt) (interface{}, error) {
	avm.mtx.Lock()
	defer avm.mtx.Unlock()
	if value, err := vm.Run(statements, avm.env); err != nil {
		return nil, err
	} else {
		return value.Interface(), nil
	}
}

func (avm *ankoVM) InterpretFunction(statements []ast.Stmt) (vm.Func, error) {
	if result, err := avm.Run(statements); err != nil {
		return nil, err
	} else {
		avm.mtx.Lock()
		defer avm.mtx.Unlock()
		if resultFunc, ok := result.(vm.Func); ok {
			return resultFunc, nil
		}
		return nil, nonFuncError
	}
}

func (avm *ankoVM) ExecuteFile(path string) (interface{}, error) {
	if b, err := ioutil.ReadFile(path); err != nil {
		return nil, err
	} else {
		return avm.Execute(string(b))
	}
}

func (avm *ankoVM) ForkVM() *ankoVM {
	return &ankoVM{env: avm.env.ForkEnv()}
}
