package gomachine

import (
	"bytes"
	"fmt"
	"net/http"
)

func getBeatHandler(ms *httpServer, hgp *httpGetDataParams, w *ResponseWriter) {
	if hgp.Key == "" {
		w.WriteJSON(ms.m.DirectGetBeatNames(hgp.Did))
	} else {
		result, found := ms.m.DirectGetBeat(hgp.Did, hgp.Key)
		w.WriteJSON(newHttpGetDataResponse(result, found))
	}
}

func putBeatHandler(ms *httpServer, hpp *httpPutBeatParams, w *ResponseWriter) {
	if err := ms.m.DirectPutBeat(hpp.Did, hpp.Key, hpp.Value); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("Error registering beat: %s", err)))
	} else {
		w.WriteJSON(true)
	}
}

func deleteBeatHandler(ms *httpServer, hgp *httpGetDataParams, w *ResponseWriter) {
	if hgp.Key == "" {
		ms.m.DirectClearBeats(hgp.Did)
	} else {
		ms.m.DirectDeleteBeat(hgp.Did, hgp.Key)
	}
	w.WriteJSON(true)
}

func binBeatPostDeleteHandler(ms *httpServer, nw *ResponseWriter, r *http.Request) {
	if hgp, err := newHttpGetDataParams(r.Body); err != nil {
		nw.WriteHeader(http.StatusInternalServerError)
		nw.Write([]byte(fmt.Sprintf("Error parsing request body: %s", err)))
		return
	} else {
		switch r.Method {
		case "POST":
			getBeatHandler(ms, hgp, nw)
		case "DELETE":
			deleteBeatHandler(ms, hgp, nw)
		}
	}
}

func binBeatPutHandler(ms *httpServer, nw *ResponseWriter, r *http.Request) {
	if hpp, err := newHttpPutBeatParams(r.Body); err != nil {
		nw.WriteHeader(http.StatusInternalServerError)
		nw.Write([]byte(fmt.Sprintf("Error parsing request body: %s", err)))
		return
	} else {
		putBeatHandler(ms, hpp, nw)
	}
}

func binBeatHandler(ms *httpServer, w http.ResponseWriter, r *http.Request) {
	if !ms.checkStopped(w, r) {
		return
	}
	if !ms.checkMethods([]string{"POST", "PUT", "DELETE"}, w, r) {
		return
	}
	if !ms.checkContentType("application/json", w, r) {
		return
	}
	if !ms.parseForm(w, r) {
		return
	}
	nw := NewResponseWriter(w)
	switch r.Method {
	case "POST", "DELETE":
		binBeatPostDeleteHandler(ms, nw, r)
	case "PUT":
		binBeatPutHandler(ms, nw, r)
	}
}

func binExecutionHandler(ms *httpServer, w http.ResponseWriter, r *http.Request) {
	if !ms.checkStopped(w, r) {
		return
	}
	if !ms.checkMethod("POST", w, r) {
		return
	}
	if !ms.checkContentType("application/json", w, r) {
		return
	}
	if !ms.parseForm(w, r) {
		return
	}
	hebp, err := newHttpExecuteBeatParams(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error parsing request body: %s", err)))
		return
	}
	nw := NewResponseWriter(w)
	context := map[string]interface{}{"response": nw, "request": NewRequest(r)}
	if result, err := ms.m.DirectContextBeat(hebp.Did, hebp.Beat, context, hebp.Params...); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error executing beat: %s", err)))
	} else {
		if result == nil {
			return
		}
		switch result := result.(type) {
		case bytes.Buffer:
			nw.Write(result.Bytes())
		case string:
			nw.Write([]byte(result))
		default:
			nw.WriteJSON(result)
		}
	}
}
