package gomachine

import (
	"fmt"
	"os"
	"sync"
)

type MachineHost struct {
	m            *Machine
	s            *Shell
	mtx          sync.Mutex
	machineName  string
	machine      *Machine
	shell        *Shell
	started      bool
	shellEnabled bool
	shellConfig  bool
	startup      []LifetimeCallback
}

func NewMachineHost(startup ...LifetimeCallback) *MachineHost {
	mh := &MachineHost{startup: startup}
	return mh
}

func (mh *MachineHost) setup() {
	ParseFlags()
	if mh.machineName == "" {
		mh.machineName = GetMachineNameFlag()
	}
	if !mh.shellConfig {
		mh.shellEnabled = GetShellFlag()
	}
	if machineName == "" {
		fmt.Println("No machine name specified")
		os.Exit(1)
	}
	mh.machine = NewMachine(machineName, mh.startup...)
	if mh.shellEnabled {
		mh.shell = NewShell(mh.machine)
	}
}

func (mh *MachineHost) SetMachineName(name string) {
	mh.mtx.Lock()
	defer mh.mtx.Unlock()
	mh.machineName = name
}

func (mh *MachineHost) GetMachineName() string {
	return mh.machineName
}

func (mh *MachineHost) SetShellEnabled(enabled bool) {
	mh.mtx.Lock()
	defer mh.mtx.Unlock()
	mh.shellEnabled = enabled
	mh.shellConfig = true
}

func (mh *MachineHost) GetShellEnabled() bool {
	return mh.shellEnabled
}

func (mh *MachineHost) GetMachine() *Machine {
	return mh.machine
}

func (mh *MachineHost) Run() {
	mh.mtx.Lock()
	defer mh.mtx.Unlock()
	mh.setup()
	if mh.shell != nil {
		mh.shell.Start()
	}
	mh.machine.MainLoop()
	mh.shell = nil
	mh.machine = nil
}
