package gomachine

import (
	"fmt"
	"github.com/pborman/uuid"
	"gomachine/address"
	"gomachine/operations"
	"reflect"
	"sync"
)

type vmInquiry struct {
	startDid   address.Did
	started    bool
	m          *Machine
	count      *operations.Count
	waiter     *operations.Wait
	vm         *ankoVM
	isStopped  bool
	Uid        string
	master     Inquiry
	parent     Inquiry
	vmStop     *VMCode
	vmExec     *VMCode
	vmAccum    *VMCode
	vmComplete *VMCode
	depth      int
	dataCount  int
	data       map[string]interface{}
	mtx        sync.Mutex
	binDid     address.Did
	err        error
}

func newVMInquiry(did address.Did, depth int, m *Machine) Inquiry {
	i := &vmInquiry{Uid: uuid.New(), startDid: did, m: m, depth: depth, vm: m.newChildVM(),
		waiter: operations.NewWait()}
	i.count = operations.NewCount(i.OnOperationsComplete)
	i.master = i
	return i
}

func (vi *vmInquiry) Did() address.Did {
	return vi.startDid
}

func (vi *vmInquiry) String() string {
	return fmt.Sprint("Inquiry-", vi.Uid)
}

func (vi *vmInquiry) Machine() *Machine {
	return vi.m
}

func (vi *vmInquiry) Depth() int {
	return vi.depth
}

func (vi *vmInquiry) SetDepth(depth int) {
	vi.depth = depth
}

func (vi *vmInquiry) stop(bin Bin) bool {
	if vi.vmStopFunc(bin) {
		return true
	}
	depth := vi.Depth()
	switch depth {
	case UnlimitedBroadcast:
		return false
	case 0:
		return true
	default:
		return false
	}
}

func (vi *vmInquiry) clone() Broadcast {
	ret := &vmInquiry{Uid: vi.Uid, parent: vi, vm: vi.vm, vmStop: vi.vmStop, vmExec: vi.vmExec, vmAccum: vi.vmAccum,
		vmComplete: vi.vmComplete, data: nil, depth: vi.depth, m: vi.m, started: true, waiter: vi.waiter,
		count: vi.count, master: vi.master}
	return ret
}

func (vi *vmInquiry) cloneInquiry() Broadcast {
	ret := &vmInquiry{Uid: vi.Uid, parent: vi.parent, vm: vi.vm.NewChildEnv(), vmStop: vi.vmStop, vmExec: vi.vmExec,
		vmAccum: vi.vmAccum, vmComplete: vi.vmComplete, data: nil, depth: vi.depth, master: vi.master,
		waiter: vi.waiter, count: vi.count, m: vi.m, started: true}
	return ret
}

func (vi *vmInquiry) spawn() Broadcast {
	depth := vi.Depth()
	if depth <= 1 && depth != UnlimitedBroadcast {
		return nil
	}
	clone := vi.clone()
	if depth != UnlimitedBroadcast {
		clone.SetDepth(depth - 1)
	}
	return clone
}

func (vi *vmInquiry) Busy() bool {
	return vi.count.Busy()
}

func (vi *vmInquiry) Wait() {
	if vi.count.Busy() {
		vi.waiter.Wait()
	}
}

func (vi *vmInquiry) StartOperation() {
	vi.count.StartOperation()
}

func (vi *vmInquiry) OperationComplete() {
	vi.count.OperationComplete()
}

func (vi *vmInquiry) OnOperationsComplete() {
	vi.complete()
	vi.vm.Destroy()
	vi.waiter.Notify()
}

func (vi *vmInquiry) Start() error {
	if vi.started {
		return nil
	}
	vi.started = true
	return vi.m.directBroadcast(vi)
}

func (vi *vmInquiry) internalLock(bin Bin) {
	vi.mtx.Lock()
	bin.Lock()
}

func (vi *vmInquiry) internalUnlock(bin Bin, completeOperation bool) {
	bin.Unlock()
	vi.mtx.Unlock()
	if completeOperation {
		vi.OperationComplete()
	}
}

func (vi *vmInquiry) broadcastStep(bin Bin) {
	vi.internalLock(bin)
	defer bin.OperationComplete()
	defer vi.internalUnlock(bin, true)
	vi.binDid = bin.Did()
	if vi.stop(bin) {
		return
	}
	vi.execute(bin)
	if spawned, ok := vi.spawn().(*vmInquiry); spawned != nil && ok {
		for _, child := range bin.Children() {
			clone := spawned.cloneInquiry()
			child.Broadcast(clone)
		}
	}
}

func (vi *vmInquiry) Root() bool {
	return vi.parent == nil
}

func (vi *vmInquiry) GetStop() *VMCode {
	return vi.vmStop
}

func (vi *vmInquiry) SetStop(f string) error {
	if f == "" {
		vi.vmStop = nil
		return nil
	}
	if vmcode, err := stringToVMCode(vi.vm, f); err != nil {
		return err
	} else {
		vi.vmStop = vmcode
	}
	return nil
}

func (vi *vmInquiry) toVMCodeValueArgs(args ...interface{}) []reflect.Value {
	ret := make([]reflect.Value, len(args))
	for pos, arg := range args {
		ret[pos] = reflect.ValueOf(arg)
	}
	return ret
}

func (vi *vmInquiry) stopRecover(bin Bin) {
	if err := recover(); err != nil {
		fmt.Println(vi, "caught error on stop at", bin, " - ", err)
		vi.Stop(fmt.Errorf("%s caught error on stop at %s - %s", vi, bin, err))
	}
}

func (vi *vmInquiry) vmStopFunc(bin Bin) bool {
	if vi.Stopped() {
		return true
	}
	if vi.vmStop == nil {
		return false
	}
	nb := newCurrentBin(bin)
	defer nb.cleanup()
	defer vi.stopRecover(bin)
	vi.vm.Define("bin", (Bin)(nb))
	vi.vm.Define("inquiry", (Inquiry)(vi))
	if result, err := vi.vm.Run(vi.vmStop.AST); err != nil {
		fmt.Println("Error running Stop", vi.vmStop, "from", vi, "in", bin, "-", err)
		vi.Stop(err)
		return true
	} else {
		switch r := result.(type) {
		case bool:
			return r
		default:
			fmt.Println("Non-bool Stop return value from", vi, "in", bin, "-", r)
			return true
		}
	}
}

func (vi *vmInquiry) GetExecute() *VMCode {
	return vi.vmExec
}

func (vi *vmInquiry) SetExecute(f string) error {
	if f == "" {
		vi.vmExec = nil
		return nil
	}
	if vmcode, err := stringToVMCode(vi.vm, f); err != nil {
		return err
	} else {
		vi.vmExec = vmcode
	}
	return nil
}

func (vi *vmInquiry) execRecover(bin Bin) {
	if err := recover(); err != nil {
		fmt.Println(vi, "caught error on execute at", bin, " - ", err)
		vi.Stop(fmt.Errorf("%s caught error on execute at %s - %s", vi, bin, err))
	}
}

func (vi *vmInquiry) execute(bin Bin) {
	if vi.vmExec == nil {
		return
	}
	nb := newCurrentBin(bin)
	defer nb.cleanup()
	defer vi.execRecover(bin)
	vi.vm.Define("bin", (Bin)(nb))
	vi.vm.Define("inquiry", (Inquiry)(vi))
	if _, err := vi.vm.Run(vi.vmExec.AST); err != nil {
		fmt.Println("Error running Execute", vi.vmExec, "from", vi, "in", bin, "-", err)
		vi.Stop(err)
	}
}

func (vi *vmInquiry) Stopped() bool {
	if vi.Root() {
		return vi.isStopped
	} else {
		return vi.master.Stopped()
	}
}

func (vi *vmInquiry) doStop(err error) {
	vi.mtx.Lock()
	defer vi.mtx.Unlock()
	vi.isStopped = true
	vi.err = err
}

func (vi *vmInquiry) Stop(err error) {
	if vi.Root() {
		go vi.doStop(err)
	} else {
		vi.master.Stop(err)
	}
}

func (vi *vmInquiry) GetVMValue(key string) (interface{}, bool) {
	if value, err := vi.vm.Get(key); err != nil {
		return nil, false
	} else {
		return value, true
	}
}

func (vi *vmInquiry) PutVMValue(key string, value interface{}) {
	vi.vm.Define(key, value)
}

func (vi *vmInquiry) Put(key string, value interface{}) {
	vi.loadDataIfBlank()
	vi.data[key] = value
	vi.dataCount = len(vi.data)
}

func (vi *vmInquiry) GetKeys() []string {
	vi.loadDataIfBlank()
	keys := make([]string, vi.dataCount)
	pos := 0
	for key := range vi.data {
		keys[pos] = key
		pos += 1
	}
	return keys
}

func (vi *vmInquiry) Get(key string) (interface{}, bool) {
	vi.loadDataIfBlank()
	value, ok := vi.data[key]
	return value, ok
}

func (vi *vmInquiry) Delete(key string) {
	vi.loadDataIfBlank()
	if _, ok := vi.data[key]; ok {
		delete(vi.data, key)
		vi.dataCount = len(vi.data)
	}
}

func (vi *vmInquiry) ClearData() {
	vi.data = nil
}

func (vi *vmInquiry) loadDataIfBlank() {
	if vi.data == nil {
		vi.data = make(map[string]interface{})
	}
}

func (vi *vmInquiry) accumRecover(bin Bin) {
	if err := recover(); err != nil {
		fmt.Println(vi, "caught error on accumulate at", bin, " - ", err)
		vi.Stop(fmt.Errorf("%s caught error on accumulate at %s - %s", vi, bin, err))
	}
}

func (vi *vmInquiry) doAccumulate(from address.Did, data interface{}, bin Bin) {
	nb := newCurrentBin(bin)
	defer nb.cleanup()
	defer vi.accumRecover(bin)
	vi.vm.Define("bin", (Bin)(nb))
	vi.vm.Define("inquiry", (Inquiry)(vi))
	vi.vm.Define("from", from)
	vi.vm.Define("data", data)
	if _, err := vi.vm.Run(vi.vmAccum.AST); err != nil {
		fmt.Println("Error running Accumulate", vi.vmAccum, "from", vi, "in", bin, "-", err)
		vi.Stop(err)
	}
}

func (vi *vmInquiry) accumulate(from address.Did, data interface{}) {
	if vi.vmAccum == nil || vi.Stopped() {
		return
	}
	localBin := vi.m.loadBin(vi.binDid)
	vi.internalLock(localBin)
	defer vi.internalUnlock(localBin, true)
	vi.doAccumulate(from, data, localBin)
}

func (vi *vmInquiry) unlockedAccumulate(from address.Did, data interface{}) {
	if vi.vmAccum == nil || vi.Stopped() {
		return
	}
	vi.doAccumulate(from, data, vi.m.loadBin(vi.binDid))
}

func (vi *vmInquiry) AccumulateToParent(data interface{}) {
	if !vi.Root() {
		if parent, ok := vi.parent.(*vmInquiry); ok {
			parent.StartOperation()
			go parent.accumulate(vi.binDid, data)
		}
	} else {
		vi.StartOperation()
		go vi.accumulate(vi.binDid, data)
	}
}

func (vi *vmInquiry) AccumulateToMaster(data interface{}) {
	if master, ok := vi.master.(*vmInquiry); ok {
		master.StartOperation()
		if !vi.Root() {
			go master.accumulate(vi.binDid, data)
		} else {
			go master.accumulate(vi.binDid, data)
		}
	}
}

func (vi *vmInquiry) unlockedAccumulateToParent(data interface{}) {
	if !vi.Root() {
		if parent, ok := vi.parent.(*vmInquiry); ok {
			parent.StartOperation()
			go parent.accumulate(vi.binDid, data)
		}
	} else {
		vi.StartOperation()
		vi.unlockedAccumulate(vi.binDid, data)
	}
}

func (vi *vmInquiry) unlockedAccumulateToMaster(data interface{}) {
	if master, ok := vi.master.(*vmInquiry); ok {
		master.StartOperation()
		if !vi.Root() {
			go master.accumulate(vi.binDid, data)
		} else {
			master.unlockedAccumulate(vi.binDid, data)
		}
	}
}

func (vi *vmInquiry) GetAccumulate() *VMCode {
	return vi.vmAccum
}

func (vi *vmInquiry) SetAccumulate(f string) error {
	if f == "" {
		vi.vmAccum = nil
		return nil
	}
	if vmcode, err := stringToVMCode(vi.vm, f); err != nil {
		return err
	} else {
		vi.vmAccum = vmcode
	}
	return nil
}

func (vi *vmInquiry) GetComplete() *VMCode {
	return vi.vmComplete
}

func (vi *vmInquiry) SetComplete(f string) error {
	if f == "" {
		vi.vmComplete = nil
		return nil
	}
	if vmcode, err := stringToVMCode(vi.vm, f); err != nil {
		return err
	} else {
		vi.vmComplete = vmcode
	}
	return nil
}

func (vi *vmInquiry) completeRecover(bin Bin) {
	if err := recover(); err != nil {
		fmt.Println(vi, "caught error on complete at", bin, " - ", err)
		vi.Stop(fmt.Errorf("%s caught error on complete at %s - %s", vi, bin, err))
	}
}

func (vi *vmInquiry) complete() {
	if vi.vmComplete == nil || (vi.Stopped() && !vi.Root()) {
		return
	}
	localBin := vi.m.loadBin(vi.binDid)
	vi.internalLock(localBin)
	defer vi.internalUnlock(localBin, false)
	nb := newCurrentBin(localBin)
	defer nb.cleanup()
	defer vi.completeRecover(localBin)
	vi.vm.Define("bin", (Bin)(nb))
	ci := newCompletingInquiry(vi)
	vi.vm.Define("inquiry", (Inquiry)(ci))
	if _, err := vi.vm.Run(vi.vmComplete.AST); err != nil {
		fmt.Println("Error running Complete", vi.vmComplete, "from", vi, "in", localBin, "-", err)
		vi.Stop(err)
		return
	}
	for _, acc := range ci.accumulates {
		vi.StartOperation()
		if acc.parent {
			vi.unlockedAccumulateToParent(acc.data)
		} else {
			vi.unlockedAccumulateToMaster(acc.data)
		}
	}
}

func (vi *vmInquiry) Error() error {
	return vi.err
}
