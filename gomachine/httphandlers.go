package gomachine

import (
	"fmt"
	"net/http"
)

type HttpFunc func(*Machine, http.ResponseWriter, *http.Request)
type ViewFunc func(*httpServer, http.ResponseWriter, *http.Request)

type HttpHandler struct {
	m *Machine
	f HttpFunc
}

func httpFuncPanicHandler(w http.ResponseWriter) {
	if err := recover(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprint(err)))
	}
}

func MachineHttpFuncWrapper(m *Machine, f HttpFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer httpFuncPanicHandler(w)
		f(m, w, r)
	}
}

func NewMachineHttpHandler(m *Machine, f HttpFunc) *HttpHandler {
	return &HttpHandler{m: m, f: f}
}

func (mhh *HttpHandler) ServeHttp(w http.ResponseWriter, r *http.Request) {
	mhh.f(mhh.m, w, r)
}

func MachineViewFuncWrapper(ms *httpServer, f ViewFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer httpFuncPanicHandler(w)
		f(ms, w, r)
	}
}
