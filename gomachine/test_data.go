package gomachine

import "gomachine/address"

// Test bin layout
var bin1Did address.Did = []interface{}{1}
var bin11Did address.Did = []interface{}{1, 1}
var bin12Did address.Did = []interface{}{1, 2}
var bin13Did address.Did = []interface{}{1, 3}
var bin111Did address.Did = []interface{}{1, 1, 1}
var bin112Did address.Did = []interface{}{1, 1, 2}
var bin113Did address.Did = []interface{}{1, 1, 3}
var bin121Did address.Did = []interface{}{1, 2, 1}
var bin122Did address.Did = []interface{}{1, 2, 2}
var bin123Did address.Did = []interface{}{1, 2, 3}
var bin131Did address.Did = []interface{}{1, 3, 1}
var bin132Did address.Did = []interface{}{1, 3, 2}
var bin133Did address.Did = []interface{}{1, 3, 3}

var layer3Dids address.DidSlice = address.DidSlice{bin111Did, bin112Did, bin113Did, bin121Did, bin122Did, bin123Did,
	bin131Did, bin132Did, bin133Did}
var layer2Dids address.DidSlice = address.DidSlice{bin11Did, bin12Did, bin123Did}
var layer1Dids address.DidSlice = address.DidSlice{bin1Did}
var allLayerDids address.DidSlice = address.DidSlice{bin1Did, bin11Did, bin12Did, bin123Did, bin111Did, bin112Did,
	bin113Did, bin121Did, bin122Did, bin123Did, bin131Did, bin132Did, bin133Did}

// Test data
const intKey string = "test"
const intValue int = 123
const strKey string = "key"
const strValue string = "value"

var dataKeys []string = []string{"test", "key"}

// Test Beats
const testBeatName string = "testBeat"
const testBeatBody string = `func (bin, a, b) { bin.Put("result", (a + b)); }`
