Bullshit
==

This folder contains ramblings related to the thinking behind the architectural decisions arbitrarily made by the
creator of the thing.

Not everything is accurate, and hopefully some of it is benchmarked (most of it might not be testable!). You have been
warned. Here be dragons. On LSD. Naked, as most dragons are (at least in the dragon-related fiction I have read). Why
are you still reading this?

Contents
--

1. [gomachine:shell](shell/README.md)
2. [gomachine:web](web/README.md)