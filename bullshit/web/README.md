gomachine:web
==

Goals
--

1. Concurrent
2. Scalable
3. Runs requests in Bins as beats
4. Loggable - replayable for development
5. Pluggable access control - [rbac](https://github.com/mikespook/gorbac)
6. Developer-friendly - lol