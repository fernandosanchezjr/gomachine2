import json
import threading
import urllib
import urlparse

import requests
import requests_unixsocket


class HTTPClient(object):

    def __init__(self, address, port=None):
        self.address = address
        if port is not None:
            self.address = "%s:%d" % (self.address, port)
        self.session = requests.Session()
        self.lock = threading.Lock()

    def get_full_url(self, url):
        return urlparse.urljoin(self.address, url)

    def request(self, method, url, **kwargs):
        self.lock.acquire()
        r = self.session.request(method, self.get_full_url(url), **kwargs)
        self.lock.release()
        return r

    def head(self, url, **kwargs):
        return self.request("HEAD", url, **kwargs)

    def get(self, url, **kwargs):
        return self.request("GET", url, **kwargs)

    def post(self, url, **kwargs):
        return self.request("POST", url, **kwargs)

    def post_json(self, url, data):
        return self.post(url, data=json.dumps(data), headers={'Content-Type': 'application/json'})

    def put(self, url, **kwargs):
        return self.request("PUT", url, **kwargs)

    def put_json(self, url, data):
        return self.put(url, data=json.dumps(data), headers={'Content-Type': 'application/json'})

    def patch(self, url, **kwargs):
        return self.request("PATCH", url, **kwargs)

    def patch_json(self, url, data):
        return self.patch(url, data=json.dumps(data), headers={'Content-Type': 'application/json'})

    def delete(self, url, **kwargs):
        return self.request("DELETE", url, **kwargs)

    def delete_json(self, url, data):
        return self.delete(url, data=json.dumps(data), headers={'Content-Type': 'application/json'})


# noinspection PyMissingConstructor
class DomainSocketClient(HTTPClient):

    def __init__(self, path):
        self.path = "http+unix://%s" % urllib.quote_plus(path)
        self.session = requests_unixsocket.Session()
        self.lock = threading.Lock()

    def get_full_url(self, url):
        if not url.startswith('/'):
            url = "/%s" % url
        return "%s%s" % (self.path, url)
