import struct


def ubyte(b):
    return ["%x" % ord(i) for i in struct.pack(">B", b)]


def ushort(b):
    return ["%x" % ord(i) for i in struct.pack(">H", b)]


def uint(i):
    return ["%x" % ord(i) for i in struct.pack(">I", i)]


def ulong(l):
    return ["%x" % ord(i) for i in struct.pack(">Q", l)]


def number(n):
    if n <= 0xff:
        return ubyte(n)
    elif n <= 0xffff:
        return ushort(n)
    elif n <= 0xffffffff:
        return uint(n)
    else:
        return ulong(n)


def join(prefix, postfix):
    new_list = prefix[:]
    new_list.extend(postfix)
    return new_list