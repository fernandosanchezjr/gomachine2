import json

from httpclient import HTTPClient, DomainSocketClient


class Client(object):

    def __init__(self, address=None, port=None, socket_file=None):
        if address is None and port is None and socket_file is None:
            e = Exception("Must provide at least an address or a socket file!")
            raise e
        if address is not None:
            self.http_client = HTTPClient(address=address, port=port)
        elif socket_file is not None:
            self.http_client = DomainSocketClient(socket_file)

    @staticmethod
    def __chunked_json_generator(it):
        for entry in it:
            yield json.loads(entry)

    def __put_data(self, prefix, did, key, value):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        if not isinstance(key, (str, basestring, unicode)):
            e = Exception("Key must be a string")
            raise e
        r = self.http_client.put_json("/machine/%s" % prefix, {"did": did, "key": key, "value": value})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return json.loads(r.text)

    def __get_data(self, prefix, did, key):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        if not isinstance(key, (str, basestring, unicode)):
            e = Exception("Key must be a string")
            raise e
        r = self.http_client.post_json("/machine/%s" % prefix, {"did": did, "key": key})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return json.loads(r.text)

    def __delete_data(self, prefix, did, key):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        if not isinstance(key, (str, basestring, unicode)):
            e = Exception("Key must be a string")
            raise e
        r = self.http_client.delete_json("/machine/%s" % prefix, {"did": did, "key": key})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return json.loads(r.text)

    def __get_keys(self, prefix, did):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        r = self.http_client.post_json("/machine/%s" % prefix, {"did": did})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return json.loads(r.text)

    def __clear_data(self, prefix, did):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        r = self.http_client.delete_json("/machine/%s" % prefix, {"did": did})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return json.loads(r.text)

    def contents(self, did, depth=3):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        r = self.http_client.post_json("/machine/contents", {"did": did, "depth": depth})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return self.__chunked_json_generator(r.iter_lines())

    def put_data(self, did, key, value):
        return self.__put_data("data", did, key, value)

    def get_data(self, did, key):
        return self.__get_data("data", did, key)

    def delete_data(self, did, key):
        return self.__delete_data("data", did, key)

    def clear_data(self, did):
        return self.__clear_data("data", did)

    def get_data_keys(self, did):
        return self.__get_keys("data", did)

    def put_meta(self, did, key, value):
        return self.__put_data("meta", did, key, value)

    def get_meta(self, did, key):
        return self.__get_data("meta", did, key)

    def delete_meta(self, did, key):
        return self.__delete_data("meta", did, key)

    def clear_meta(self, did):
        return self.__clear_data("meta", did)

    def get_meta_keys(self, did):
        return self.__get_keys("meta", did)

    def put_beat(self, did, key, value):
        return self.__put_data("beats", did, key, value)

    def get_beat(self, did, key):
        return self.__get_data("beats", did, key)

    def delete_beat(self, did, key):
        return self.__delete_data("beats", did, key)

    def clear_beat(self, did):
        return self.__clear_data("beats", did)

    def get_beat_names(self, did):
        return self.__get_keys("beats", did)

    def execute_beat(self, did, beat_name, *args):
        if not isinstance(did, list):
            e = Exception("DID must be a list")
            raise e
        if not isinstance(beat_name, (str, basestring, unicode)):
            e = Exception("Beat name must be a string")
            raise e
        r = self.http_client.delete_json("/machine/beats/execute", {"did": did, "beat": beat_name})
        if r.status_code != 200:
            e = Exception(r.text)
            raise e
        else:
            return json.loads(r.text)