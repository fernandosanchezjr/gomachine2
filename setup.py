#!/usr/bin/env python

from setuptools import setup


setup(name='pygomachine',
      provides='pygomachine',
      version='0.1',
      description='Python client for gomachine:',
      author='Fernando Sanchez, Jr.',
      author_email='fernando.sanchez.jr@gmail.com',
      url='https://bitbucket.org/fernandosanchezjr/gomachine2',
      packages=['pygomachine'],
      install_requires=['requests-unixsocket>=0.1.5'],
      zip_safe=False)
