package main

import (
	"gomachine"
	"neuronmachinelib"
)

func main() {
	mh := gomachine.NewMachineHost(neuronmachinelib.SetupTree)
	mh.Run()
}
