package main

import (
	"gomachine"
)

func main() {
	mh := gomachine.NewMachineHost()
	mh.Run()
}
